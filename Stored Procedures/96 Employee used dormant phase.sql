USE [BSKAssociates]
GO
/****** Object:  StoredProcedure [dbo].[DeltekStoredProc_sp_Bishop_96_Emp_used_Dormant_Phase]    Script Date: 8/2/2019 9:01:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[DeltekStoredProc_sp_Bishop_96_Emp_used_Dormant_Phase] 
	-- Add the parameters for the stored procedure here
	@startdate datetime,
	@enddate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	; 

SELECT
tk.Employee
	, tk.WBS1
    , tk.RegHrs
    , tk.OvtHrs
	, tk.LaborCode
	, tk.SpecialOvtHrs
	, tk.TransDate
	, em.LastName
	, em.FirstName
	, em.JobCostType
	, tk.WBS2
	, tk.WBS3
	, pr.WBS2
	, pr.WBS3
	, em.EMail
	, em.Type
	, pr.LongName
	, ectf.cust5XHolidayPremiumAmount
	, ectf.cust6XSickTimePremiumAmount
	, ectf.cust7XVacationPremiumAmount
	, org.LabDistWBS2
	, pr.CostRateTableNo
	, tk.TransComment
	, ''
	, ''
FROM tkDetail AS tk
JOIN EM AS em ON tk.Employee = em.Employee
JOIN PR AS pr ON tk.WBS1 = pr.WBS1 AND tk.WBS2 = pr.WBS2 AND tk.WBS3 = pr.WBS3
JOIN EmployeeCustomTabFields AS ectf ON tk.Employee = ectf.Employee
JOIN Organization AS org ON em.Org = org.Org                        
WHERE tk.TransDate >= @startDate AND tk.TransDate <= @endDate AND (tk.WBS2 in ('222','224','226','229','356','357','358','359','418','407','405','411','403','404','417','416','402','406'))   and (org.LabDistWBS2 IN ('OVH000000','OVHSICK00','OVHHOLI00','OVHLTSICK', 'OVHMARKET', 'OBHPROFDE')) 
END
