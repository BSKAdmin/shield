# Project SHIELD 
## Project Scope
SHIELD is a connector to Deltek using the Vision Xtend API 

### Deployed Environments
- [Dev Environment](http://shield.bskdev.com) on dev-web
- Staging Not Deployed
- Production Not Deployed

### Projects using SHIELD
- PWXML Program (In Progress)
- SharePoint Dashboard (Not Started)
- Metafield Data Sync (In Progress)
- Bishop (Not Started)
- Agent Coulson - Stored Procedure Runner View (Completed)
- Project Management Dashboard (New)

# Shield.Console Usage
Using the Shield.Console is fairly easy. The name of the executable may be different. Depends on the install. The usage is very simple as it takes in 2 arguments. Arg0 is the name of the recipient and arg1 is there email. By default the email will go to IT. 
> Shield.Console.exe arg0 arg1
EX:
```powershell
Shield.Console.exe "John Doe" "JDoe@bskassociates.com"
```
# Notes
## Metafield Data Sync
(5/31/2018) - Cameron Gose

Talked with Cheri about marking projects as Metafield Projects in Deltek. This will allow the syncing of project and etc. to be targeted.
> Cheri marked active CSD projects to be Metafield Projects. All future CSD projects will automatically be marked as Metafield Projects
TODO: Need to see how to pull those projects

> Cheri will be talking with Tim next Monday discussing what Geo projects need to be marked as Metafield Projects. Geo projects will not be marked as Metafield Projects by default.
TODO: Follow up with Cheri next week

> Deadline June 30th
