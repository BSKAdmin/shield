﻿using Shield.DAL.ProjectSync;
using Shield.DAL.ProjectSync.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;

namespace Shield.Services.DataSync
{
    public static class LogService
    {
        private static ShieldSyncContext _context = new ShieldSyncContext();

        public static async Task LogError<T>(string error, List<T> items)
        {
            var dataAsJson = Json.Encode(items);
            var log = new SyncLog()
            {
                Error = error,
                ItemsEffected = dataAsJson,
                Occured = DateTime.Now,
                Type = typeof(T).ToString()
            };
            _context.Logs.Add(log);
            await _context.SaveChangesAsync();
        }
    }
}