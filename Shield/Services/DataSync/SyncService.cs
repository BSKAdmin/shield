﻿using Shield.DAL.Deltek.Models;
using Shield.DAL.MetaField.Repositories;
using Shield.DAL.ProjectSync;
using Shield.DAL.ProjectSync.Models;
using Shield.Services.Deltek;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Shield.Services.SharePoint;

namespace Shield.Services.DataSync
{
    public class SyncService
    {
        private ProjectService _projects;
        private ClientService _clients;
        private ShieldSyncContext _shield;
        private EmployeeService _employee;
        private ContactService _contacts;
        private GenericRepository<DAL.MetaField.Models.Project> _projectRepo = new GenericRepository<DAL.MetaField.Models.Project>("projects");
        private GenericRepository<DAL.MetaField.Models.WBS> _wbsRepo = new GenericRepository<DAL.MetaField.Models.WBS>("wbs");
        private GenericRepository<DAL.MetaField.Models.Client> _clientRepo = new GenericRepository<DAL.MetaField.Models.Client>("clients");
        private GenericRepository<DAL.MetaField.Models.Employee> _employeeRepo = new GenericRepository<DAL.MetaField.Models.Employee>("employees");
        private GenericRepository<DAL.MetaField.Models.Contact> _contactRepo = new GenericRepository<DAL.MetaField.Models.Contact>("contacts");
        private static int STEPPER = 25;
        private List<Branch> _branches;
        private SharePointProjectFolderStrategy _sharePointProjectFolderStrategy;
        private FileSystemProjectFolderStrategy _fileSystemProjectFolderService;
        private SharePointFolderService _sharePointFolderService;


        public SyncService()
        {
            _projects = new ProjectService();
            _clients = new ClientService();
            _shield = new ShieldSyncContext();
            _employee = new EmployeeService();
            _contacts = new ContactService();
            _branches = _shield.Branches.ToList();
            _sharePointFolderService = new SharePointFolderService();
            _fileSystemProjectFolderService = new FileSystemProjectFolderStrategy();
            _sharePointProjectFolderStrategy = new SharePointProjectFolderStrategy();
        }

        public async Task<Run> RunSync()
        {
            var lastRun = GetLastRun();
            //var lastRun = GetLastRun(new DateTime(2019, 3, 1), "Running to catch up data sync with change");

            var run = StartRun();
            try
            {
                List<Project> deltekProjects;                
                //Initialize if no data in database
                Initialize();
                //Update Existing Data
                //Add new data
                if (lastRun != null)
                {
                    UpdateDeltekClients(_clients.GetClients(lastRun.Start));
                    UpdateDeltekContacts(_contacts.GetContact(lastRun.Start));
                    deltekProjects = _projects.GetProjects(lastRun.Start).GroupBy(pr => pr.WBS1).Select(gr => gr.First()).ToList();                    
                }
                else
                {
                    deltekProjects = _projects.GetProjects();
                }

                UpdateDeltekProjects(deltekProjects);
                UpdateEmployees(_employee.GetEmployees());
                                
                if (lastRun != null)
                {
                    SyncNewData(deltekProjects, run);
                }
                await SyncDataToMetafield();               
            }
            catch (Exception e)
            {
                run = LogRunError(run, e.ToString());
            }
            finally
            {            
                 run = EndRun(run);
                _shield.Run.Add(run);
                _shield.SaveChanges();                
            }
            return run;
        }

        public void Initialize()
        {
            InitializeProjectSync();
           
            InitializeClientSync();
            
            InitializeContactSync();
            
        }

        private void InitializeContactSync()
        {
            if (!_shield.Contacts.Any())
            {
                var clientIds = _shield.Client.Select(c => c.Id).ToList();
                var deltekContacts = _contacts.GetContact(clientIds);
                AddDeltekContacts(deltekContacts);
            }
        }

        private void InitializeClientSync()
        {
            if (!_shield.Client.Any())
            {
                var clientIds = _shield.Project.Select(p => p.ClientID).Distinct().ToList();
                var deltekClients = _clients.GetClients(clientIds);
                AddDeltekClients(deltekClients);

            }
        }

        private void InitializeProjectSync()
        {
            if (!_shield.Project.Any())
            {
                var deltekProjects = _projects.GetProjects();
                AddDeltekProjects(deltekProjects);
            }
        }

        public async Task SyncDataToMetafield()
        {
            await SyncEmployeesToMetafield();
            await SyncClientsToMetafield();
            await SyncProjectsToMetafield();
            await SyncWBSToMetafield();
            await SyncContactsToMetafield();            
        }
        public async Task SyncEmployeesToMetafield()
        {
            var employees = _shield.Employees.Where(ee => ee.NeedsSync);
            var count = 0;
            while(count <= employees.Count())
            {
                var employeesToSync = employees.OrderBy(ee => ee.EmployeeID).Skip(count).Take(STEPPER).ToList();
                count += STEPPER;
                List<DAL.MetaField.Models.Employee> metafieldEmployees = new List<DAL.MetaField.Models.Employee>();
                employeesToSync.ForEach(e => metafieldEmployees.Add(MapToMetafieldEmployee(e)));

                try
                {
                    //Send to Metafield
                    var result = await _employeeRepo.Post(metafieldEmployees);
                    if (result)
                    {
                        //Update local projects needsSync = false
                        employeesToSync.ForEach(p => p.NeedsSync = false);
                        employeesToSync.ForEach(p => UpdateSyncEmployee(p));
                        _shield.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public async Task SyncProjectsToMetafield()
        {
            var count = 0;
            var projects = _shield.Project.Where(p => p.NeedsSync && p.WBS2 == "" && p.WBS3 == "" && p.IsMetafield /*&& p.City != null && p.ProjMgr != null && p.State != null && p.Address1 != null*/).OrderBy(p => p.WBS1);
            var numOfProjects = projects.Count();
            while (count <= numOfProjects)
            {
                //Get WBS1 only which is the root project                
                var projectsToSync = projects.Skip(count).Take(STEPPER).ToList();
                count += STEPPER;
                //Map to Metafield Project
                List<DAL.MetaField.Models.Project> metafieldProjects = new List<DAL.MetaField.Models.Project>();
                
                try
                {
                    bool l_error = false;
                    projectsToSync.ForEach(async (p) => {
                        l_error = await IntegrityCheck(p, projectsToSync);
                        metafieldProjects.Add(MapToMetafieldProject(p));                            
                    });

                    //Send to Metafield
                    if (metafieldProjects.Any())
                    {
                        if (!l_error)
                        {
                            var result = await _projectRepo.Post(metafieldProjects);
                            if (result)
                            {
                                //Update local projects needsSync = false
                                projectsToSync.ForEach(p => p.NeedsSync = false);
                                projectsToSync.ForEach(p => UpdateSyncedProject(p));
                                _shield.SaveChanges();
                            }
                        }
                    }
                }
                catch(Exception e)
                {
                    throw e;
                }
            }
        }        

        public async Task SyncWBSToMetafield()
        {
            //iterate through up to <STEPPER> different items
            //Post to metafield
            //empty array and get another <STEPPER>.
            List<DAL.MetaField.Models.WBS> metafieldWBSs = new List<DAL.MetaField.Models.WBS>();
            List<SyncedProject> allProjects = new List<SyncedProject>();
            //Get WBS2s
            //var wbs2Projects = _shield.Project.Where(p => p.NeedsSync && p.WBS2 != "" && p.WBS3 == "").OrderBy(p => p.Name).ToList();
            //need to look at wbs3 in wbs2 with no updates.            
            var wbs2Projects = _shield.Project.Where(p => p.WBS2 != "" && p.WBS3 == "" /*&& p.ProjMgr != null*/).OrderBy(p => p.WBS1).ThenBy(p => p.WBS2).ToList();            
            //Foreach WBS2
            foreach (var wbs2 in wbs2Projects)
            {
                //Only sync if parent project synced.
                if (!_shield.Project.Any(p => p.WBS1 == wbs2.WBS1 && p.WBS2 == "" && p.WBS3 == "" && p.NeedsSync))
                {
                    if (await HandleMetaFieldWBSs(metafieldWBSs, allProjects))
                    {
                        allProjects = new List<SyncedProject>();
                        metafieldWBSs = new List<DAL.MetaField.Models.WBS>();
                    }

                    //Add WBS2 to list       
                    if (wbs2.NeedsSync)
                    {
                        allProjects.Add(wbs2);
                        metafieldWBSs.Add(MapToMetafieldWBS(wbs2, WBS.WBS2));
                    }

                    //Get WBS3's that belong to WBS2
                    var wbs3Projects = _shield.Project.Where(p => p.NeedsSync && p.WBS1 == wbs2.WBS1 && p.WBS2 == wbs2.WBS2 && p.WBS3 != "").ToList();
                    foreach (var wbs3 in wbs3Projects)
                    {
                        allProjects.Add(wbs3);
                        metafieldWBSs.Add(MapToMetafieldWBS(wbs3, WBS.WBS3));
                        if (await HandleMetaFieldWBSs(metafieldWBSs, allProjects))
                        {
                            allProjects = new List<SyncedProject>();
                            metafieldWBSs = new List<DAL.MetaField.Models.WBS>();
                        }
                    }
                }
            }

            if (metafieldWBSs.Any())
            {
                var result = await _wbsRepo.Post(metafieldWBSs);
                if (result)
                {
                    allProjects.ForEach(p => p.NeedsSync = false);
                    allProjects.ForEach(p => UpdateSyncedProject(p));
                    _shield.SaveChanges();
                }
            }
        }

        /// <summary>
        /// If metafieldData count is equal to STEPPER then post data to metafield and return new empty list. Otherwise return initial list 
        /// </summary>
        /// <param name="metafieldData"></param>
        /// <returns></returns>
        private async Task<bool> HandleMetaFieldWBSs(List<DAL.MetaField.Models.WBS> metafieldData, List<SyncedProject> projectsToSync)
        {
            if (metafieldData.Count() >= STEPPER)
            {
                var result = await _wbsRepo.Post(metafieldData);
                ////Update local projects needsSync = false
                if (result)
                {
                    projectsToSync.ForEach(p => p.NeedsSync = false);
                    projectsToSync.ForEach(p => UpdateSyncedProject(p));
                    _shield.SaveChanges();                    
                }
                return true;
            }
            return false;
        }

        public async Task SyncClientsToMetafield()
        {
            var count = 0;
            while (count <= _shield.Client.Count(p => p.NeedsSync))
            {
                //Get WBS1 only which is the root project                
                var clientsToSync = _shield.Client.Where(p => p.NeedsSync).OrderBy(p => p.Name).Skip(count).Take(STEPPER).ToList();
                count += STEPPER;
                //Map to Metafield Project
                List<DAL.MetaField.Models.Client> metafieldClients = new List<DAL.MetaField.Models.Client>();
                clientsToSync.ForEach(c => metafieldClients.Add(MapToMetafieldClient(c)));
                try
                {
                    //Send to Metafield
                    if (await _clientRepo.Post(metafieldClients))
                    {
                        //Update local projects needsSync = false
                        clientsToSync.ForEach(p => p.NeedsSync = false);
                        clientsToSync.ForEach(c => UpdateSyncedClient(c));
                        _shield.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public async Task SyncContactsToMetafield()
        {
            var count = 0;
            var contacts = _shield.Contacts.Where(p => p.NeedsSync).OrderBy(p => p.LastName);
            while (count < contacts.Count())
            {

                var contactsToSync = contacts.Skip(count).Take(STEPPER).ToList();
                
                //Map to Metafield Contact
                List<DAL.MetaField.Models.Contact> metafieldContacts = new List<DAL.MetaField.Models.Contact>();
                contactsToSync.ForEach(c => metafieldContacts.Add(MapToMetafieldContact(c)));
                try
                {
                    //Send to Metafield
                    if (await _contactRepo.Post(metafieldContacts))
                    {
                        //Update local projects needsSync = false
                        contactsToSync.ForEach(p => p.NeedsSync = false);
                        contactsToSync.ForEach(c => UpdateSyncedContact(c));
                        _shield.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                count += STEPPER;
            }
        }

        private DAL.MetaField.Models.Employee MapToMetafieldEmployee(SyncedEmployee e)
        {
            return new DAL.MetaField.Models.Employee()
            {
                id = e.EmployeeID,
                eMail = e.EMail,
                firstName = e.FirstName,
                lastName = e.LastName,
                status = DetermineStatus(e.Status),
                isFieldTechnician = false,
                isLabTechnician = false,
                isProjectManager = false
            };
        }

        private DAL.MetaField.Models.WBS MapToMetafieldWBS2(SyncedProject p)
        {
            return new DAL.MetaField.Models.WBS()
            {
                id = p.WBS2,
                projectID = p.WBS1,
                hierarchyNumber = 1,
                parentID = null,
                status = "Active",
                name = p.Name,
            };
        }

        private DAL.MetaField.Models.Client MapToMetafieldClient(SyncedClient c)
        {
            return new DAL.MetaField.Models.Client()
            {
                id = c.Id,
                address = c.Address,
                city = c.City,
                name = c.Name,
                postalCode = c.PostalCode,
                state = c.State,
                status = "Active"                
            };

        }

        private DAL.MetaField.Models.Contact MapToMetafieldContact(SyncedContact c)
        {
            return new DAL.MetaField.Models.Contact()
            {
                id = c.Id,
                clientID = c.ClientID,
                eMail = c.EMail,
                firstName = c.FirstName,
                lastName = c.LastName,
                status = c.Status,
                workPhone = c.WorkPhone
            };
        }

        public DAL.MetaField.Models.Project MapToMetafieldProject(SyncedProject project)
        {
            return new DAL.MetaField.Models.Project()
            {
                id = project.WBS1,
                address = project.Address1,
                address2 = project.Address2,
                city = project.City,
                clientID = project.ClientID,
                clientProjectNumber = project.ProjectNumber,
                code = project.WBS1,
                name = project.Name,
                state = project.State,
                serviceDescription = project.Description ?? "TODO: Update in Deltek",
                postalCode = project.Zip,
                status = DetermineStatus(project.Status),
                projectOwnerID = project.ProjMgr,
                projectManagerID = project.ProjMgr,
                
            };
        }
        private enum WBS
        {
            WBS2,
            WBS3
        }
        private DAL.MetaField.Models.WBS MapToMetafieldWBS(SyncedProject project, WBS wbs)
        {
            switch (wbs)
            {
                case WBS.WBS2:
                    return new DAL.MetaField.Models.WBS()
                    {
                        id = $"{project.WBS1}{project.WBS2}",
                        name = project.Name,                        
                        projectID = project.WBS1,
                        code = project.WBS2,
                        //parentCode = project.WBS1,
                        status = "Active",
                        hierarchyNumber = 1, 
                    };
                case WBS.WBS3:
                    return new DAL.MetaField.Models.WBS()
                    {
                        id = $"{project.WBS1}{project.WBS2}{project.WBS3}",
                        parentID = $"{project.WBS1}{project.WBS2}",
                        name = project.Name,
                        code = project.WBS3,
                        parentCode = project.WBS2,
                        projectID = project.WBS1,
                        status = "Active",
                        hierarchyNumber = 2, 
                    };
                default:
                    return null;
            }
        }

        public void UpdateEmployees(List<Employee> employees)
        {
            foreach (var ee in employees)
            {
                if (!_shield.Employees.Any(e => ee.EmployeeID == e.EmployeeID))
                {
                    _shield.Employees.Add(EmployeeMap(ee));
                }
                //else
                //{
                //    //if (_shield.Employees.Any(e => e.EmployeeID == ee.EmployeeID && e.ModDate != ee.ModDate))
                //    //{
                //    //    UpdateSyncEmployee(EmployeeMap(ee));
                //    //}
                //}
            }
            _shield.SaveChanges();
        }            

        public void UpdateDeltekProjects(List<DAL.Deltek.Models.Project> deltekProjects)
        {
            //Update Projects
            //get projects from deltek that have been synced before and have been updated compared to last data in shield.             

            var updatedDeltekProjects = deltekProjects.Where(p => _shield.Project
                                                                .Any(dp => dp.WBS1 == p.WBS1 &&
                                                                           dp.WBS2 == p.WBS2 &&
                                                                           dp.WBS3 == p.WBS3)).ToList();

            //var updatedDeltekProjects = deltekProjects.Where(p => _shield.Project
            //    .Any(dp => dp.WBS1 == p.WBS1 &&
            //               dp.WBS2 == p.WBS2 &&
            //               dp.WBS3 == p.WBS3)).ToList();

            //Map Deltek projects to SyncedProject
            List<SyncedProject> projectsToUpdate = new List<SyncedProject>();
            updatedDeltekProjects.ForEach(p => projectsToUpdate.Add(ProjectMap(p)));

            projectsToUpdate.ForEach(p => UpdateSyncedProject(p));
            _shield.SaveChanges();
        }

        public void AddDeltekProjects (List<DAL.Deltek.Models.Project> deltekProjects)
        {
            var count = 0;
            List<SyncedProject> projectsToSync = new List<SyncedProject>();
            deltekProjects.ForEach(c => projectsToSync.Add(ProjectMap(c)));
            //Save in chuncks. Initially there is a lot of data.
            while (count <= projectsToSync.Count())
            {
                _shield.Project.AddRange(projectsToSync.Skip(count).Take(STEPPER));
                _shield.SaveChanges();
                count += STEPPER;
            }
        }

        public void UpdateDeltekClients(List<DAL.Deltek.Models.Client> deltekClients)
        {
            var updatedClients = deltekClients.Where(c => _shield.Client.Any(sc => sc.Id == c.Id && c.ModDate != sc.ModDate)).ToList();

            List<SyncedClient> clientsToUpdate = new List<SyncedClient>();
            updatedClients.ForEach(c => clientsToUpdate.Add(ClientMap(c)));
            clientsToUpdate.ForEach(c => UpdateSyncedClient(c));
            _shield.SaveChanges();
        }

        public void AddDeltekClients(List<DAL.Deltek.Models.Client> deltekClients)
        {
            var count = 0;
            List<SyncedClient> clientsToSync = new List<SyncedClient>();
            deltekClients.ForEach(c => clientsToSync.Add(ClientMap(c)));
            //Save in chuncks. Initially there is a lot of data.
            while (count <= clientsToSync.Count())
            {
                _shield.Client.AddRange(clientsToSync.Skip(count).Take(STEPPER));
                _shield.SaveChanges();
                count += STEPPER;
            }
        }

        public void UpdateDeltekContacts(List<DAL.Deltek.Models.Contact> deltekContacts)
        {
            var updatedContacts = deltekContacts.Where(c => _shield.Contacts.Any(sc => sc.ClientID == c.ClientID && c.ModDate != sc.ModDate)).ToList();
            List<SyncedContact> contactsToSync = new List<SyncedContact>();
            updatedContacts.ForEach(c => contactsToSync.Add(ContactMap(c)));
            contactsToSync.ForEach(c => UpdateSyncedContact(c));
            
        }
        

        public void AddDeltekContacts(List<DAL.Deltek.Models.Contact> deltekContacts)
        {
            var count = 0;
            List<SyncedContact> contactsToSync = new List<SyncedContact>();
            deltekContacts.ForEach(c => contactsToSync.Add(ContactMap(c)));
            //Save in chuncks. Initially there is a lot of data.
            while (count <= contactsToSync.Count())
            {
                _shield.Contacts.AddRange(contactsToSync.Skip(count).Take(STEPPER));
                _shield.SaveChanges();
                count += STEPPER;
            }
        }

        public void ForceUpdateProjects(List<DAL.Deltek.Models.Project> deltekProjects)
        {
            foreach (var deltekProject in deltekProjects)
            {
                if (!_shield.Project.Any(p => p.WBS1 == deltekProject.WBS1 &&
                                              p.WBS2 == deltekProject.WBS2 &&
                                              p.WBS3 == deltekProject.WBS3))
                {
                    _shield.Project.Add(ProjectMap(deltekProject));
                }
            }
            //Update Projects
            //get projects from deltek that have been synced before and have been updated compared to last data in shield.             
            var updatedDeltekProjects = deltekProjects.Where(p => _shield.Project
                .Any(dp => dp.WBS1 == p.WBS1 &&
                           dp.WBS2 == p.WBS2 &&
                           dp.WBS2 == p.WBS3)).ToList();

            //Map Deltek projects to SyncedProject
            List<SyncedProject> projectsToUpdate = new List<SyncedProject>();
            updatedDeltekProjects.ForEach(p => projectsToUpdate.Add(ProjectMap(p)));

            projectsToUpdate.ForEach(p => UpdateSyncedProject(p));
            _shield.SaveChanges();
        }

        public void UpdateData(List<DAL.Deltek.Models.Project> deltekProjects, List<DAL.Deltek.Models.Client> deltekClients)
        {
            //Update Projects
            //get projects from deltek that have been synced before and have been updated compared to last data in shield.             
            var updatedDeltekProjects = deltekProjects.Where(p => _shield.Project
                                                                .Any(dp => dp.WBS1 == p.WBS1 &&
                                                                           dp.WBS2 == p.WBS2 &&
                                                                           dp.WBS2 == p.WBS3 &&
                                                                           dp.ModDate != p.ModDate)).ToList();

            //Map Deltek projects to SyncedProject
            List<SyncedProject> projectsToUpdate = new List<SyncedProject>();
            updatedDeltekProjects.ForEach(p => projectsToUpdate.Add(ProjectMap(p)));

            projectsToUpdate.ForEach(p => UpdateSyncedProject(p));
            _shield.SaveChanges();

            
            if(deltekClients != null)
            {
                var updatedClients = deltekClients.Where(c => _shield.Client.Any(sc => sc.Id == c.Id && c.ModDate != sc.ModDate)).ToList();

                List<SyncedClient> clientsToUpdate = new List<SyncedClient>();
                updatedClients.ForEach(c => clientsToUpdate.Add(ClientMap(c)));
                clientsToUpdate.ForEach(c => UpdateSyncedClient(c));
                _shield.SaveChanges();

                
                
            }
        }

        /// <summary>
        /// Returns Active by default
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        private string DetermineStatus(string status)
        {
            switch (status)
            {
                case "A":
                    return "Active";
                case "I":
                    return "Inactive";
                default:
                    return "Active";
            }
        }

        private void UpdateSyncedProject(SyncedProject p)
        {
            _shield.Project.Attach(p);
            _shield.Entry(p).State = EntityState.Modified;
        }
        private void UpdateSyncedClient(SyncedClient c)
        {
            _shield.Client.Attach(c);
            _shield.Entry(c).State = EntityState.Modified;
        }

        private void UpdateSyncedContact(SyncedContact c)
        {
            if (!_shield.Contacts.Any(e => e.Id == c.Id))
            {
                _shield.Contacts.Add(c);
            }
            else
            {            
                _shield.Contacts.Attach(c);
                _shield.Entry(c).State = EntityState.Modified;                
            }
            try
            {
                _shield.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
                //Do nothing for now
            }
        }

        private void UpdateSyncEmployee(SyncedEmployee e)
        {
            _shield.Employees.Attach(e);
            _shield.Entry(e).State = EntityState.Modified;
        }

        public async Task<bool> IntegrityCheck(SyncedProject p_Project, List<SyncedProject> p_Projects)
        {
            if (p_Project.ProjMgr == string.Empty || p_Project.ProjMgr == null)
            {
                await LogService.LogError<SyncedProject>($"{p_Project.WBS1}{p_Project.WBS2}{p_Project.WBS3} - {p_Project.Name} is missing a manager", p_Projects);
                return true;
            }
            return false;
        }

        private void UpdateExistingProjects()
        {
            //Get projects from deltek
            var deltekProjects = _projects.GetProjects();

            //CompareSyncedProjects(deltekProjects);
            
        }
       

        /// <summary>
        /// Syncs only new data that does't currently exist in shield.
        /// </summary>
        /// <remarks>
        /// If there is no value for lastRun it will collect all projects for metafield.
        /// This is good for the first run.
        /// </remarks>
        /// <param name="lastRun"></param>
        public void SyncNewData(List<Project> deltekProjects, Run run)
        {            
            //Get data into SHIELD
            List<SyncedProject> projectsToSync = new List<SyncedProject>();
            //Get new projects and group then by Project Number WBS1
            List<Project> NewProjects = DiscoverNewProjects(deltekProjects);
            NewProjects.ForEach(p => projectsToSync.Add(ProjectMap(p, true)));
            DiscoverNewWBS(deltekProjects)
                .ForEach(p => projectsToSync.Add(ProjectMap(p, true)));
            //Convert and save to SHIELD database.
            _shield.Project.AddRange(projectsToSync);
            _shield.SaveChanges();

            //Create Folders
            NewProjects.Where(proj => proj.WBS2 == " " && proj.WBS3 == " ").ToList().ForEach(p => CreateFolder(p));
                      
            List<string> clientIds = DiscoverNewClients();            
            //Get clients and their contacts
            List<SyncedClient> clientsToSync = new List<SyncedClient>();
            List<SyncedContact> contactsToSync = new List<SyncedContact>();
            _clients.GetClients(clientIds)
                .ForEach(c => clientsToSync.Add(ClientMap(c)));

            _shield.Client.AddRange(clientsToSync);
            _shield.SaveChanges();
            //Get new contacts based on clients in shield database            
            DiscoverNewContacts(run)
                .ForEach(c => contactsToSync.Add(ContactMap(c)));

            
            _shield.Contacts.AddRange(contactsToSync);
            _shield.SaveChanges();
        }

        public void CreateFolder(Project p_Project)
        {
            var projectIdentifier = p_Project.WBS1.Last().ToString().ToUpper();
            var branch = _branches.FirstOrDefault(b => b.ProjectIdentifier == projectIdentifier);
            if (branch == null) return;
           
            switch (branch.FolderStrategy)
            {
                case FolderStrategy.FileSystem:
                    _fileSystemProjectFolderService.CreateProjectFolders(p_Project);
                    return;
                case FolderStrategy.SharePoint:
                    var projectFolder = _sharePointProjectFolderStrategy.CreateProjectFolders(p_Project);
                    _sharePointFolderService.CreateProjectFolder(projectFolder);
                    return;
                default:
                    return;
            }
            
        }

        private List<DAL.Deltek.Models.Contact> DiscoverNewContacts(Run run)
        {            
            var deltekContacts = _contacts.GetContact(run.Start);
            //Return deltek contacts that don't have a matching Id in shield contacts and have a matching clientId
            return deltekContacts.Where(dc => !_shield.Contacts.Any(sc => sc.Id == dc.Id) && _shield.Client.Any(sc => sc.Id == dc.ClientID)).ToList();
        }

        public List<Project> DiscoverNewWBS(List<Project> deltekProjects)
        {
            var shieldPhases = _shield.Project.Select(p => new { p.WBS1, p.WBS2, p.WBS3 });
            //Gets new WBS2 & WBS3 that have been assigned to an existing project
            var newWBS2 = deltekProjects.Where(p => shieldPhases.Select(s => s.WBS1).Contains(p.WBS1) && !shieldPhases.Select(s => s.WBS2).Contains(p.WBS2)).ToList();
            //Gets new WBS3 that have been assigned to a projects WBS2
            var newWBS3 = deltekProjects.Where(p => shieldPhases.Select(s => s.WBS1).Contains(p.WBS1) &&
                                                     shieldPhases.Select(s => s.WBS2).Contains(p.WBS2) &&
                                                     !shieldPhases.Select(s => s.WBS3).Contains(p.WBS3)).ToList();
            List<Project> result = new List<Project>();
            result.AddRange(newWBS2);
            result.AddRange(newWBS3);
            return result;
        }

        public List<Project> DiscoverNewProjects(List<Project> deltekProjects)
        {
            var shieldIds = _shield.Project.Select(p => p.WBS1).Distinct().ToList();
            var newProjects = deltekProjects.Where(p => !shieldIds.Contains(p.WBS1)).ToList();

            return newProjects;
        }

        /// <summary>
        /// This is ran after DiscoverNewProjects. Looks at projects in SyncedProjects that do not have a matching client in SyncedClient
        /// </summary>
        /// <returns>List<string></returns>
        public List<string> DiscoverNewClients()
        {            
            //Returns project clientids that don't match up with a id in the client table
            var clientIds = from project in _shield.Project
                            join client in _shield.Client on project.ClientID equals client.Id into gj
                            from projectclient in gj.DefaultIfEmpty()
                            where projectclient == null
                            select project.ClientID;

            return clientIds.ToList();
        }

        
        private SyncedContact ContactMap(Contact c)
        {
            return new SyncedContact() {
                Id = c.Id,
                ClientID = c.ClientID,
                EMail = c.EMail,
                FirstName = c.FirstName,
                LastName = c.LastName,
                ModDate = c.ModDate,
                NeedsSync = true,
                Status = DetermineStatus(c.Status),
                WorkPhone = c.WorkPhone
            };
        }

        private SyncedClient ClientMap(Client c)
        {
            return new SyncedClient() {
                Address = c.Address,
                City = c.City,
                Id = c.Id,
                Name = c.Name,
                PostalCode = c.ZIP,
                State = c.State,
                ModDate = c.ModDate,
                NeedsSync = true                
            };            
        }
        private SyncedEmployee EmployeeMap(Employee employee, bool needsSync = true)
        {
            return new SyncedEmployee()
            {
                EmployeeID = employee.EmployeeID,
                EMail = employee.EMail,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                NeedsSync = needsSync,
                ModDate = employee.ModDate,
                Status = employee.Status
            };
        }

        private SyncedProject ProjectMap(Project project, bool needsSync = true)
        {
            return new SyncedProject()
            {
                Address1 = project.Address1,
                Address2 = project.Address2,
                City = project.City,
                State = project.State,
                Zip = project.Zip,
                ClientID = project.ClientID,
                Description = project.Description,
                Name = project.Name,
                ProjMgr = project.ProjMgr,
                Status = project.Status,
                Supervisor = project.Supervisor,
                WBS1 = project.WBS1,
                WBS2 = project.WBS2,
                WBS3 = project.WBS3,
                ModDate = project.ModDate,
                NeedsSync = needsSync,
                ProjectType = project.ProjectType,
                IsMetafield = project.CustMetafieldProject == "Y",
                ProjectNumber = project.CustMetafieldClientProjectNo
            };
        }
        private void SaveProject(Project project)
        {            
            _shield.Project.Add(ProjectMap(project));
            _shield.SaveChanges();
        }

        private Run GetLastRun() => _shield.Run.OrderByDescending(r => r.Start).FirstOrDefault();

//        private Run GetLastRun() => new Run(){ Start = new DateTime(2015, 10, 01), End = new DateTime(2015, 10, 01)};

        private Run GetLastRun(DateTime p_Start,  string p_Message) => new Run()
        {
            Start = p_Start,            
            Error = p_Message
        };
        private Run EndRun(Run run) => new Run() { Start = run.Start, End = DateTime.Now };
        private Run LogRunError(Run run, string error) => new Run() { Start = run.Start, End = run.End, Error = error };
        private Run StartRun() => new Run() { Start = DateTime.Now };

    }
}