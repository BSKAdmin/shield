﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Shield.DAL.Deltek.Models;

namespace Shield.Services
{
    public class FileSystemProjectFolderStrategy
    {
        public void CreateProjectFolders(Project project)
        {

            string l_Path = ConfigurationManager.AppSettings["ProjectDirectory"];
            switch (project.WBS1.Last())
            {
                case 'F':
                    l_Path += @"\FRS\Active\";
                    break;
                case 'B':
                    l_Path += @"\BFLD\Active\";
                    break;
                case 'S':
                    l_Path += @"\SAC\Active\";
                    break;
                case 'P':
                case 'L':
                    l_Path += @"\LVM\Active\";
                    break;
                default:
                    return;
            }

            switch (project.WBS1.First())
            {
                case 'G':
                case 'g':
                    l_Path += @"GEO\";
                    break;
                case 'E':
                case 'e':
                    l_Path += @"ENV\";
                    break;
                case 'C':
                case 'c':
                    l_Path += @"CSD\";
                    break;
            }

            try
            {
                
                if (System.IO.Directory.Exists(l_Path) && System.IO.Directory.GetDirectories(l_Path, $@"{project.WBS1}*").Any()) return;
                l_Path += $@"{project.WBS1} - {project.Name.Replace("/", " ")}";
                System.IO.Directory.CreateDirectory(l_Path);
                System.IO.Directory.CreateDirectory(l_Path + @"\Communication");
                System.IO.Directory.CreateDirectory(l_Path + @"\Contracts");
                System.IO.Directory.CreateDirectory(l_Path + @"\Data");
                System.IO.Directory.CreateDirectory(l_Path + @"\Deliverables");
                System.IO.Directory.CreateDirectory(l_Path + @"\Financial");
                System.IO.Directory.CreateDirectory(l_Path + @"\Graphics");
                System.IO.Directory.CreateDirectory(l_Path + @"\Photos");
                System.IO.Directory.CreateDirectory(l_Path + @"\Proposals");
                System.IO.Directory.CreateDirectory(l_Path + @"\Supporting Documents");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}