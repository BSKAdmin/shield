﻿using Shield.DAL.Deltek.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Shield.Services.Deltek
{
    public class ProjectService
    {        
        private DeltekService _deltek;

        public ProjectService()
        {
            _deltek = new DeltekService();
        }

        public List<Project> GetProjects(DateTime? startTime = null)
        {
            var deltekProjects = new List<Project>();

            deltekProjects = _deltek.GetResourceFromStoredProcedure<Project>("sp_MetaFieldGetProjects", null, true, startTime: startTime);
            return deltekProjects;
        }

        public List<Project> GetProject(string WBS1)
        {
            var deltekProjects = new List<Project>();
            string query = $"SELECT PR.*, PCTF.* FROM PR JOIN ProjectCustomTabFields pctf ON pctf.WBS1 = PR.WBS1 AND pctf.WBS2 = PR.WBS2 AND pctf.WBS3 = PR.WBS3 WHERE PR.WBS1 = '{WBS1}' AND CustMetafieldProject = 'Y'";
            deltekProjects = _deltek.GetResource<Project>(VisionInfoCenters.Projects, query, RecordDetail.All);
            return deltekProjects;
        }
    }
}