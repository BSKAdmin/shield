﻿using Shield.DAL.Deltek.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Shield.Services.Deltek
{
    public class VendorService
    {        
        DeltekService _deltek;
        public VendorService()
        {
            _deltek = new DeltekService();
        }

        /// <summary>
        /// Search for vendors
        /// </summary>
        /// <param name="nameSearch"></param>
        /// <returns></returns>
        public List<VendorAddress> GetVendorAddress(string nameSearch)
        {
            List<VendorAddress> deltekVendors = new List<VendorAddress>();
            if (!nameSearch.Any()) return deltekVendors;

            var query = $"SELECT TOP(10) VEAddress.* FROM dbo.VE JOIN dbo.VEAddress ON VEAddress.Vendor = VE.Vendor WHERE Status = 'A' AND PrimaryInd = 'Y' AND VE.Name LIKE '{ nameSearch }%'";            
            deltekVendors.AddRange(_deltek.GetResource<VendorAddress>(VisionInfoCenters.Vendors, query, RecordDetail.All));            
            return deltekVendors;
        }

        public VendorAddress GetVendor(string key)
        {
            if (!key.Any()) return null;

            var query = $"SELECT * FROM dbo.VE where VE.Vendor = '{key}'";
            var result = _deltek.GetResource<VendorAddress>(VisionInfoCenters.Vendors, query, RecordDetail.All).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// Query for multiple vendor keys
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<VendorAddress> GetVendors(IEnumerable<string> keys)
        {
            List<VendorAddress> deltekVendors = new List<VendorAddress>();
            if (!keys.Any()) return null;            
            var query = $"SELECT * FROM dbo.VE where ";
            foreach(var key in keys)
            {
                if (key == keys.Last())
                {
                    query += $"VE.Vendor = '{key}'";
                }
                else
                {
                    query += $"VE.Vendor = '{key}' OR ";
                }
            }
            deltekVendors.AddRange(_deltek.GetResource<VendorAddress>(VisionInfoCenters.Vendors, query, RecordDetail.All));
            return deltekVendors;
        }
    }
}