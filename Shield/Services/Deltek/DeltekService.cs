﻿using Shield.DAL.Deltek.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Shield.Services.Deltek
{
    
    public class DeltekService
    {
        private VisionAPIRepository _repository;

        public DeltekService()
        {

            _repository = new VisionAPIRepository(
                     ConfigurationManager.AppSettings["VisionWSUrl"],
                     ConfigurationManager.AppSettings["VisionDatabase"],
                     ConfigurationManager.AppSettings["VisionUser"],
                     ConfigurationManager.AppSettings["VisionPassword"]);

        }

        /// <summary>
        /// Fetches data from Vision XTend API using stored procedures 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedureName"></param>
        /// <param name="page">Specify if stored procedure will be paging through results.</param>
        /// <param name="offset">Used for paging through results 100 by default</param>
        /// <returns></returns>
        public XDocument GetResourceFromStoredProcedure(string storedProcedureName, Dictionary<string, object> parameters = null)
        {
            VisionMessage l_VisionMessage = _repository.ExecuteStoredProcedure(storedProcedureName, parameters);
            return l_VisionMessage.ReturnedData;                
        }

        /// <summary>
        /// Fetches data from Vision XTend API using stored procedures 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedureName"></param>
        /// <param name="page">Specify if stored procedure will be paging through results.</param>
        /// <param name="offset">Used for paging through results 100 by default</param>
        /// <returns></returns>
        public List<T> GetResourceFromStoredProcedure<T>(string storedProcedureName, Dictionary<string, object> parameters = null, bool page = false, int offset = 100, DateTime? startTime = null) where T : IXMLModel<T>, new()
        {
            var deltekObjects = new List<T>();
            var start = 0;
            if (parameters == null) parameters = new Dictionary<string, object>();
            if (page)
            {
                parameters.Add("@start", start);
                parameters.Add("@offset", offset);                
            }
            if (startTime.HasValue)
            {
                parameters.Add("@startTime", startTime.Value);
            }
            else
            {
                parameters.Add("@startTime", null);
            }
            VisionMessage l_VisionMessage;
            do
            {                
                l_VisionMessage = _repository.ExecuteStoredProcedure(storedProcedureName, parameters);

                var records = l_VisionMessage.ReturnedData.Element("NewDataSet").Elements("Table");
                foreach (var e in records)
                {
                    if (e != null)
                    {
                        deltekObjects.Add(new T().MapFromSPXML(e));
                    }
                }

                if (page)
                {
                    start += offset;
                    parameters["@start"] = start;
                }
            }
            while (l_VisionMessage.ReturnedData.Element("NewDataSet").ElementExists("Table") && page);

            return deltekObjects;
        }
        /// <summary>
        /// Fetches data from Vision XTend API GetRecordsByQuery
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="infoCenter"></param>
        /// <param name="query"></param>
        /// <param name="detail"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public List<T> GetResource<T>(VisionInfoCenters infoCenter, string query, RecordDetail detail, bool page = false, int offset = 100 ) where T : IXMLModel<T>, new()
        {
            var deltekObjects = new List<T>();
            var start = 0;
            var submittedQuery = PrepareQuery(query, page, start, offset);
            VisionMessage l_VisionMessage;
            do
            {

                try
                {
                    l_VisionMessage = _repository.GetRecordsByQuery(infoCenter, submittedQuery, detail);
                    var records = l_VisionMessage.ReturnedData.Element("RECS").Elements("REC");
                    foreach (var e in records)
                    {
                        if (e != null)
                        {
                            deltekObjects.Add(new T().MapFromXML(e));
                        }
                    }

                    start += offset;
                    submittedQuery = PrepareQuery(query, page, start, offset);                    
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            while (l_VisionMessage.ReturnedData.Element("RECS") != null && l_VisionMessage.ReturnedData.Element("RECS").Value.Any() && page);

            return deltekObjects;
        }


        private string PrepareQuery(string query, bool page, int start, int offset)
        {
            return page ? query +$" OFFSET { start } ROWS FETCH NEXT { offset } ROWS ONLY" : query;
        }
    }
}