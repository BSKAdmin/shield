﻿using Newtonsoft.Json;
using Shield.DAL.Deltek.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Shield.Services.Deltek
{
    public class EmployeeService
    {
        //Employees to be synced into Metafield
        private static readonly Dictionary<string, string> Orgs = new Dictionary<string, string> {
            { "Livermore Materials Lab", "10:04:01" },
            { "Livermore Geotechnical Svcs", "10:04:20" },
            { "Livermore Construction Svcs", "10:04:60" },
            { "Sacramento Geotechnical Svcs", "10:05:20" },
            { "Sacramento Construction Svcs", "10:05:60" },
            { "Fresno Geotechnical Svcs", "20:01:20" },
            { "Fresno Materials Lab", "20:01:01" },
            { "Fresno Construction Svcs", "20:01:60" },
            { "Bakersfield Materials Lab", "20:03:01" },
            { "Bakersfield Geotechnical Svcs", "20:03:20" },
            { "Bakersfield Construction Svcs", "20:03:60" }
        };

        private VisionAPIRepository repository;
        private DeltekService _deltek;
        public EmployeeService()
        {
            _deltek = new DeltekService();            
        }

        public List<Employee> GetEmployees()
        {
            List<Employee> deltekEmployees = new List<Employee>();            
            foreach (var org in Orgs)
            {
                var query = $"SELECT Employee, EMail, FirstName, LastName, Status FROM em WHERE Status = 'A' and EMail IS NOT NULL AND Org = '{org.Value}'";
                deltekEmployees.AddRange(_deltek.GetResource<Employee>(VisionInfoCenters.Employees, query, RecordDetail.Primary));
                
                //Different way of doing the same thing as above.
                //string obj = JsonConvert.SerializeXmlNode(doc);
                //var result = JsonConvert.DeserializeObject<Rootobject>(obj);
                //if (result.RECS.REC != null)
                //{
                //    foreach (var rec in result.RECS.REC)
                //    {
                //        if (rec != null && rec.EM != null && rec.EM.ROW != null)
                //        {
                //            var employee = rec.EM.ROW;
                //            deltekEmployees.Add(new Employee()
                //            {
                //                EmployeeID = employee.Employee,
                //                EMail = employee.EMail,
                //                FirstName = employee.FirstName,
                //                LastName = employee.LastName
                //            });
                //        }
                //    }
                //}
            }

            return deltekEmployees;
        }
    }
}