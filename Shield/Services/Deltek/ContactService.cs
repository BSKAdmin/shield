﻿using Shield.DAL.Deltek.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Shield.Services.Deltek
{
    public class ContactService
    {
        private VisionAPIRepository _repository;
        private DeltekService _deltek;

        public ContactService()
        {
            _deltek = new DeltekService();
            _repository = new VisionAPIRepository(
                 ConfigurationManager.AppSettings["VisionWSUrl"],
                 ConfigurationManager.AppSettings["VisionDatabase"],
                 ConfigurationManager.AppSettings["VisionUser"],
                 ConfigurationManager.AppSettings["VisionPassword"]);
        }

        public List<Contact> GetContact(DateTime? startDate)
        {
            if (startDate.HasValue)
            {
                var results = _deltek.GetResource<Contact>(VisionInfoCenters.Contacts, $"SELECT * FROM dbo.Contacts WHERE Contacts.EMail IS NOT NULL AND Contacts.FirstName IS NOT NULL AND Contacts.LastName IS NOT NULL AND Contacts.ModDate > '{startDate.Value}' ORDER BY Contacts.ContactID", RecordDetail.All, true);
                return results;
            }
            return new List<Contact>();
        }

        public List<Contact> GetContact(List<string> clientIds)
        {
            if (clientIds.Any())
            {
                var results = _deltek.GetResource<Contact>(VisionInfoCenters.Contacts, $"SELECT * FROM dbo.Contacts WHERE Contacts.EMail IS NOT NULL AND Contacts.FirstName IS NOT NULL AND Contacts.LastName IS NOT NULL AND ({ParseClientIds(clientIds)}) ORDER BY Contacts.ContactID", RecordDetail.Primary, true);
                return results;
            }
            return new List<Contact>();
        }

        private string ParseClientIds(List<string> clientIds)
        {
            var query = "Contacts.ClientID = '";
            for (var i = 0; i < clientIds.Count(); i++)
            {
                query = query + clientIds[i] + ((i < clientIds.Count() - 1) ? "' OR Contacts.ClientID = '" : "'");
            }
            return query;
        }
    }
}