﻿using Shield.DAL.Deltek.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;

namespace Shield.Services.Deltek
{
    public class ClientService
    {
        private VisionAPIRepository _repository;
        private DeltekService _deltek;
        public ClientService()
        {
            _deltek = new DeltekService();
            _repository = new VisionAPIRepository(
                 ConfigurationManager.AppSettings["VisionWSUrl"],
                 ConfigurationManager.AppSettings["VisionDatabase"],
                 ConfigurationManager.AppSettings["VisionUser"],
                 ConfigurationManager.AppSettings["VisionPassword"]);
        }

        public List<Client> GetClients(DateTime? startDate)
        {
            if (startDate.HasValue) {                 
                var results = _deltek.GetResource<Client>(VisionInfoCenters.Clients, $"SELECT * FROM dbo.CL JOIN dbo.CLAddress ON CLAddress.ClientID = CL.ClientID WHERE PrimaryInd = 'Y' AND CL.ModDate > '{startDate.Value}' ORDER BY CL.ClientID", RecordDetail.All, true);
                return results;
            }
            return new List<Client>();
        }
        public List<Client> GetClients(List<string> clientIds)
        {
            if (clientIds.Any())
            {
                var results = _deltek.GetResource<Client>(VisionInfoCenters.Clients, $"SELECT * FROM dbo.CL JOIN dbo.CLAddress ON CLAddress.ClientID = CL.ClientID WHERE PrimaryInd = 'Y' AND ({ParseClientIds(clientIds)}) ORDER BY CL.ClientID", RecordDetail.All, true);
                return results;
            }
            return new List<Client>();            
        }

        private string ParseClientIds(List<string> clientIds)
        {
            var query = "CL.ClientID = '";
            for (var i = 0; i < clientIds.Count(); i++)
            {
                query = query + clientIds[i] + ((i < clientIds.Count() - 1) ? "' OR CL.ClientID = '" : "'");
            }
            return query;
        }


        public List<Client> GetClients()
        {
            var results = _deltek.GetResource<Client>(VisionInfoCenters.Clients, $"SELECT * FROM dbo.CL JOIN dbo.CLAddress ON CLAddress.ClientID = CL.ClientID WHERE PrimaryInd = 'Y' ORDER BY CL.ClientID", RecordDetail.All, true);
            return results;


            //var deltekClients = new List<Client>();
            //var start = 0;
            //var offset = 500;
            //var moreData = true;
            //while (moreData)
            //{
            //    var query = $"SELECT * FROM dbo.CL JOIN dbo.CLAddress ON CLAddress.ClientID = CL.ClientID WHERE PrimaryInd = 'Y' ORDER BY CL.ClientID OFFSET { start } ROWS FETCH NEXT { offset } ROWS ONLY";
            //    VisionMessage l_VisionMessage = _repository.GetRecordsByQuery(VisionInfoCenters.Clients, query, RecordDetail.All);
            //    var xml = l_VisionMessage.ReturnedData.ToString();
            //    var records = l_VisionMessage.ReturnedData.Element("RECS").Elements("REC");
            //    if (records.Count() < offset) moreData = false;
            //    foreach (var e in records)
            //    {
            //        if (e != null)
            //        {
            //            deltekClients.Add(new Client()
            //            {
            //                Id = e.Element("CL").Element("ROW").Element("ClientID").Value,
            //                Name = e.Element("CL").Element("ROW").Element("Name").Value,
            //                Address = e.Element("CLAddress").Element("ROW").Element("Address").Value,
            //                City = e.Element("CLAddress").Element("ROW").Element("City").Value,                            
            //                State = e.Element("CLAddress").Element("ROW").Element("State").Value,
            //                ZIP = e.Element("CLAddress").Element("ROW").Element("ZIP").Value,
            //            });
            //            //NOT SURE IF THIS WORKS
            //            //var address = $"SELECT CLAddress.* FROM CLAddress WHERE ClientID = { e.Element("CL").Element("ROW").Element("ClientID").Value}";
            //            //VisionMessage l_VisionMessageAddress = _repository.GetRecordsByQuery(VisionInfoCenters.Clients, address, RecordDetail.Primary);
            //            //var addresses = l_VisionMessage.ReturnedData.Element("RECS").Elements("REC");
            //        }
            //    }
            //    start += offset;
            //}
            //return deltekClients;
        }
    }
}