﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint.Client;

namespace Shield.Services.SharePoint
{
    public class SharePointFolderService
    {
        public List<Folder> CreateProjectFolder(ProjectFolder projectFolder)
        {
            var web = projectFolder.SharePointContext.Web;
            var list = web.Lists.GetByTitle(projectFolder.DocumentList);
            try
            {
                return new List<Folder>
                    {
                        CreateInternalFolder(web, list.RootFolder, projectFolder.FolderPath + @"/Communication"),
                        CreateInternalFolder(web, list.RootFolder, projectFolder.FolderPath + @"/Contracts"),
                        CreateInternalFolder(web, list.RootFolder, projectFolder.FolderPath + @"/Data"),
                        CreateInternalFolder(web, list.RootFolder, projectFolder.FolderPath + @"/Deliverables"),
                        CreateInternalFolder(web, list.RootFolder, projectFolder.FolderPath + @"/Financial"),
                        CreateInternalFolder(web, list.RootFolder, projectFolder.FolderPath + @"/Graphics"),
                        CreateInternalFolder(web, list.RootFolder, projectFolder.FolderPath + @"/Photos"),
                        CreateInternalFolder(web, list.RootFolder, projectFolder.FolderPath + @"/Proposals"),
                        CreateInternalFolder(web, list.RootFolder, projectFolder.FolderPath + @"/Supporting Documents"),
                    };
            }
            catch (Exception ex)
            {
                //log exception
                return null;
            }
        }

        private Folder CreateInternalFolder(Web web, Folder parentFolder, string fullFolderUrl)
        {
            var foldersToCreate = fullFolderUrl.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            Folder currentFolder = parentFolder.Folders.Add(foldersToCreate[0]);
            web.Context.Load(currentFolder);
            web.Context.ExecuteQuery();
            if (foldersToCreate.Length > 1)
            {
                string subFolderUrl = string.Join("/", foldersToCreate, 1, foldersToCreate.Length - 1);
                return CreateInternalFolder(web, currentFolder, subFolderUrl);
            }
            return currentFolder;
        }
    }
}