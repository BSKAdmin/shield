﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using Microsoft.SharePoint.Client;

namespace Shield.Services.SharePoint
{
    public static class SharePointContextService
    {
        public static ClientContext GetContext()
        {
            return new ClientContext(System.Configuration.ConfigurationManager.AppSettings["SharePointUrl"])
            {
                Credentials = new SharePointOnlineCredentials(System.Configuration.ConfigurationManager.AppSettings["SharePointUser"],
                    ToSecureString(System.Configuration.ConfigurationManager.AppSettings["SharePointPassword"]))
            };
        }

        private static SecureString ToSecureString(string password)
        {
            SecureString securePassword = new SecureString();
            foreach (char character in password.ToCharArray())
            {
                securePassword.AppendChar(character);
            }
            return securePassword;
        }
    }
}