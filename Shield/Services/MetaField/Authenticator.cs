﻿using Shield.DAL.MetaField.Models;
using Shield.DAL.ProjectSync;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace Shield.Services.MetaField
{
    public static class Authenticator
    {
        private static HttpClient _client = new HttpClient();        
        private static string _baseUrl = ConfigurationManager.AppSettings["MetaFieldURL"];
        private static string _authToken = null;
        private static Credentials _apiCred = new Credentials() {
            username = ConfigurationManager.AppSettings["MetaFieldUser"],
            password = ConfigurationManager.AppSettings["MetaFieldPassword"]
        };
        public static string AuthenticationToken
        {
            get
            {
                if (_authToken != null) return _authToken;
                try
                {
                    return GetAuthToken().GetAwaiter().GetResult();
                }
                catch(Exception e)
                {
                    using (ShieldSyncContext context = new ShieldSyncContext())
                    {
                        context.Logs.Add(new DAL.ProjectSync.Models.SyncLog()
                        {
                            ItemsEffected = "bskapi user",
                            Error = $"Log in most likely failed. Try logging in as {_apiCred.username} user. Error: {e.ToString()}",
                            Occured = DateTime.Now,
                            Type = "Logon"
                        });
                        context.SaveChanges();
                    }
                    return null;
                }
            }
        }

        private static async Task<string> GetAuthToken()
        {
            _client.BaseAddress = new Uri(_baseUrl);
            var response = await _client.PostAsJsonAsync("authenticate", _apiCred);
            try
            {
                response.EnsureSuccessStatusCode();
                _authToken = await response.Content.ReadAsAsync<string>();
                return _authToken;
            }
            catch(Exception e)
            {
                throw e;
            }

        }

    }
}