﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint.Client;

namespace Shield.Services
{
    public class ProjectFolder
    {
        public string DocumentList { get; set; }
        public string FolderPath { get; set; }
        public ClientContext SharePointContext { get; set; }

        public ProjectFolder(ClientContext ctx)
        {
            SharePointContext = ctx;
            DocumentList = string.Empty;
            FolderPath = string.Empty;
        }

    }
}