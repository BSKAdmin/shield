﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Shield.DAL.Deltek.Models;
using Shield.Services.SharePoint;

namespace Shield.Services
{
    public class SharePointProjectFolderStrategy
    {
        public ProjectFolder CreateProjectFolders(Project project)
        {
            using (var ctx = SharePointContextService.GetContext())
            {
                ProjectFolder projectFolder = new ProjectFolder(ctx);

                switch (project.WBS1.Last())
                {
                    case 'F':
                        projectFolder.DocumentList = @"FRS Projects Active";
                        break;
                    case 'B':
                        projectFolder.DocumentList = @"BFLD Projects Active";
                        break;
                    case 'S':
                        projectFolder.DocumentList = @"SAC Projects Active";
                        break;
                    case 'P':
                    case 'L':
                        projectFolder.DocumentList = @"LVM Projects Active";
                        break;
                    default:
                        break;
                }

                switch (project.WBS1.First())
                {
                    case 'G':
                    case 'g':
                        projectFolder.FolderPath += @"/GEO/";
                        break;
                    case 'E':
                    case 'e':
                        projectFolder.FolderPath += @"/ENV/";
                        break;
                    case 'C':
                    case 'c':
                        projectFolder.FolderPath += @"/CSD/";
                        break;
                }

                projectFolder.FolderPath += $@"{project.WBS1} - {project.Name}";
                return projectFolder;
            }
        }
    }
}