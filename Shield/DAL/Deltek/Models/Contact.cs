﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Shield.DAL.Deltek.Models
{
    public class Contact : IXMLModel<Contact>
    {
        public string Id { get; set; }
        public string ClientID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string WorkPhone { get; set; }
        public string Status { get; set; }
        public string ModDate { get; set; }

        public Contact MapFromSPXML(XElement e)
        {
            throw new NotImplementedException();
        }

        public Contact MapFromXML(XElement e)
        {
            Id = e.Element("Contacts").Element("ROW").Element("ContactID").Value;
            ClientID= e.Element("Contacts").Element("ROW").Element("ClientID").Value;
            FirstName = e.Element("Contacts").Element("ROW").Element("FirstName").Value;
            LastName = e.Element("Contacts").Element("ROW").Element("LastName").Value;
            EMail = e.Element("Contacts").Element("ROW").Element("EMail").Value;
            WorkPhone = e.Element("Contacts").Element("ROW").Element("Phone").Value;
            Status = e.Element("Contacts").Element("ROW").Element("ContactStatus").Value;
            ModDate = e.Element("Contacts").Element("ROW").Element("ModDate").Value;
            return this;
        }
    }
}