﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Shield.DAL.Deltek.Models
{
    public class VendorAddress : IXMLModel<VendorAddress>
    {
        public string Vendor { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string AccountNumber { get; set; }
        public string ContactNumber { get; set; }

        public VendorAddress MapFromSPXML(XElement e)
        {
            throw new NotImplementedException();
        }

        public VendorAddress MapFromXML(XElement e)
        {
            Vendor = e.Element("VEAddress").Element("ROW").Element("Vendor").Value;
            Name = e.Element("VE").Element("ROW").Element("Name").Value;
            Address = e.Element("VEAddress").Element("ROW").Element("Address").Value;
            Address1 = e.Element("VEAddress").Element("ROW").Element("Address1").Value;
            City = e.Element("VEAddress").Element("ROW").Element("City").Value;
            Fax = e.Element("VEAddress").Element("ROW").Element("Fax").Value;
            Phone = e.Element("VEAddress").Element("ROW").Element("Phone").Value;
            State = e.Element("VEAddress").Element("ROW").Element("State").Value;
            ZIP = e.Element("VEAddress").Element("ROW").Element("ZIP").Value;
            AccountNumber = e.Element("VendorCustomTabFields").Element("ROW").Element("CustInfo").Value;
            ContactNumber = e.Element("VendorCustomTabFields").Element("ROW").Element("CustPhone2").Value;

            return this;
        }
    }
}