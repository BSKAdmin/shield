﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Shield.DAL.Deltek.Models
{
    public class Employee : IXMLModel<Employee>
    {        
        public string EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string Status { get; set; }
        public string ModDate { get; set; }
        public Employee MapFromSPXML(XElement e)
        {
            throw new NotImplementedException();
        }

        public Employee MapFromXML(XElement e)
        {
            EmployeeID = e.Element("EM").Element("ROW").Element("Employee").Value;
            EMail = e.Element("EM").Element("ROW").Element("EMail").Value;
            FirstName = e.Element("EM").Element("ROW").Element("FirstName").Value;
            LastName = e.Element("EM").Element("ROW").Element("LastName").Value;
            Status = e.Element("EM").Element("ROW").Element("Status").Value;
            ModDate = e.Element("EM").Element("ROW").Element("ModDate").Value;
            return this;
        }
    }
}