﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Shield.DAL.Deltek.Models
{
    public interface IXMLModel<T>
    {
        T MapFromXML(XElement e);
        /// <summary>
        /// XML Mapping from StoredProcedure is different.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        T MapFromSPXML(XElement e);
    }
}
