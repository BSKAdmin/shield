﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Shield.DAL.Deltek.Models
{
    public class Client : IXMLModel<Client>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string ModDate { get; set; }

        public Client MapFromSPXML(XElement e)
        {
            throw new NotImplementedException();
        }

        public Client MapFromXML(XElement e)
        {
            Id = e.Element("CL").Element("ROW").Element("ClientID").Value;
            Name = e.Element("CL").Element("ROW").Element("Name").Value;
            Address = e.Element("CLAddress").Element("ROW").Element("Address1").Value;
            City = e.Element("CLAddress").Element("ROW").Element("City").Value;
            State = e.Element("CLAddress").Element("ROW").Element("State").Value;
            ZIP = e.Element("CLAddress").Element("ROW").Element("ZIP").Value;
            ModDate = e.Element("CLAddress").Element("ROW").Element("ModDate").Value;
            return this;
        }
    }
}