﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Shield.DAL.Deltek.Models
{
    public class Project : IXMLModel<Project>
    {
        public string WBS1 { get; set; }
        public string WBS2 { get; set; }
        public string WBS3 { get; set; } 
        public string Name { get; set; }
        public string ProjMgr { get; set; }        
        public string ClientID { get; set; }        
        public string Status { get; set; }        
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Supervisor { get; set; }
        public string Description { get; set; }
        public string ModDate { get; set; }
        public string ProjectType { get; set; }
        public string CustMetafieldProject { get; set; }
        public string custSiteType { get; set; }
        public string custBusinessLicenseEntity { get; set; }
        public string custBranchManager { get; set; }
        public string custUSTFundProject { get; set; }
        public string custContractType { get; set; }
        public string custWageDetermination { get; set; }
        public string custLatitude { get; set; }
        public string custLongitude { get; set; }
        public string CustState { get; set; }
        public string CreateUser { get; set; }
        public string CreateDate { get; set; }
        public string ModUser { get; set; }        
        public string CustOpportunityNo { get; set; }
        public string CustFeeBasis { get; set; }
        public string CustAgreementType { get; set; }
        public string CustClientRefNo { get; set; }
        public string CustPaidwhenPaid { get; set; }
        public string CustPWPDaystoPay { get; set; }
        public string CustClientsAPPayCycle { get; set; }
        public string CustBusinessLicenseExp { get; set; }
        public string CustBusinessLicenseNo { get; set; }
        public string CustHowBSKisPaid { get; set; }
        public string CustDayInvoiceisNeededby { get; set; }
        public string CustIsBSKaSubcontractor { get; set; }
        public string CustRequiresLienRelease { get; set; }
        public string CustInvoiceCoverLetterReqd { get; set; }
        public string CustPaymentApplicationReqd { get; set; }
        public string CustOtherSpecialFormReqd { get; set; }
        public string CustReimbExpenseBackup { get; set; }
        public string CustLienReleaseSentto { get; set; }
        public string CustLienEmailAddress { get; set; }
        public string CustRetentionWithheld { get; set; }
        public string CustRetentionPercentage { get; set; }
        public string CustInvoiceInformationMemo { get; set; }
        public string CustInvoiceDeliveryMethod { get; set; }
        public string CustStampedCodedInvoiceNeededfromClient { get; set; }
        public string CustNeedInvoiceSummary { get; set; }
        public string CustNeedStatement { get; set; }
        public string CustAPEmailAddress { get; set; }
        public string CustBillingEmail { get; set; }
        public string CustBillingPhoneNo { get; set; }
        public string CustClientPhoneNo { get; set; }
        public string CustInvoiceTemplate { get; set; }
        public string CustAPPhoneNo { get; set; }
        public string CustClientCellPhone { get; set; }
        public string CustRetainerAmount { get; set; }
        public string CustRetainerRecd { get; set; }
        public string CustContactEmail { get; set; }
        public string CustAPContact { get; set; }
        public string CustDIRProjectNumber { get; set; }
        public string CustDIRContractID { get; set; }
        public string CustDIRAwardingBodyID { get; set; }
        public string CustDIRAwardingBody { get; set; }
        public string CustDIRAwardedContractor { get; set; }
        public string CustSalesRep { get; set; }
        public string CustDIRProjectID { get; set; }
        public string CustBDAssociate { get; set; }
        public string CustCPRContact { get; set; }
        public string CustCPRAddress { get; set; }
        public string CustCPRCityStateZip { get; set; }
        public string CustCPREmail { get; set; }
        public string CustCertifiedPayrollSentVia { get; set; }
        public string CustAdditionalCPRCopies { get; set; }
        public string CustDASForm140 { get; set; }
        public string CustDAS142RequestDispatch { get; set; }
        public string CustAdditionalRequirements { get; set; }
        public string CustDIRAwardingBody1 { get; set; }
        public string CustPWC100 { get; set; }
        public string CustNoPWWorkorSubsUsed { get; set; }
        public string CustPrivatelyFundedNODIRNeeded { get; set; }
        public string CustOffSiteMaterialsLabTestingONLY { get; set; }
        public string CustNOPWC100SetuoDesignPhase { get; set; }
        public string CustBSKeCPRuploadedFinaled { get; set; }
        public string CustNoDIRNumberProjectUnder1000 { get; set; }
        public string CustProjectedRevenue { get; set; }
        public string CustDirectCompensation { get; set; }
        public string CustFinalBilled { get; set; }
        public string CustStatementDelivery { get; set; }        
        public string CustLCPTrackerRequired { get; set; }
        public string CustCPRSentwithMonthlyInvoice { get; set; }
        public string CustLasteCPRUploadedWeekEndDate { get; set; }
        public string CustFirstDayPWWorked { get; set; }
        public string CustMetafieldClientProjectNo { get; set; }


        public Project MapFromSPXML(XElement e)
        {
            WBS1 = e.ElementExists("WBS1") ? e.Element("WBS1").Value : null;
            WBS2 = e.ElementExists("WBS2") ? e.Element("WBS2").Value : null;
            WBS3 = e.ElementExists("WBS3") ? e.Element("WBS3").Value : null;
            Address1 = e.ElementExists("Address1") ? e.Element("Address1").Value : null;
            Address2 = e.ElementExists("Address2") ? e.Element("Address2").Value : null;
            City = e.ElementExists("City") ? e.Element("City").Value : null;
            State = e.ElementExists("State") ? e.Element("State").Value : null;
            ProjMgr = e.ElementExists("ProjMgr") ? e.Element("ProjMgr").Value : null;
            Name = e.ElementExists("Name") ? e.Element("Name").Value : null;
            Status = e.ElementExists("Status") ? e.Element("Status").Value : null;
            ClientID = e.ElementExists("ClientID") ? e.Element("ClientID").Value : null;
            Supervisor = e.ElementExists("Supervisor") ? e.Element("Supervisor").Value : null;
            Zip = e.ElementExists("Zip") ? e.Element("Zip").Value : null;
            Description = e.ElementExists("Description") ? e.Element("Description").Value : null;
            ModDate = e.ElementExists("ModDate") ? e.Element("ModDate").Value : null;
            ProjectType = e.ElementExists("ProjectType") ? e.Element("ProjectType").Value : null;
            CustMetafieldProject = e.ElementExists("CustMetafieldProject") ? e.Element("CustMetafieldProject").Value : null;
            CustMetafieldClientProjectNo = e.ElementExists("CustMetafieldClientProjectNo") ? e.Element("CustMetafieldClientProjectNo").Value : null;
            return this;
        }

        public Project MapFromXML(XElement e)
        {
            WBS1 = e.Element("PR").Element("ROW").Element("WBS1").Value;
            WBS2 = e.Element("PR").Element("ROW").Element("WBS2").Value;
            WBS3 = e.Element("PR").Element("ROW").Element("WBS3").Value;
            Address1 = e.Element("PR").Element("ROW").Element("Address1").Value;
            Address2 = e.Element("PR").Element("ROW").Element("Address2").Value;
            City = e.Element("PR").Element("ROW").Element("City").Value;
            State = e.Element("PR").Element("ROW").Element("State").Value;
            ProjMgr = e.Element("PR").Element("ROW").Element("ProjMgr").Value;
            Name = e.Element("PR").Element("ROW").Element("Name").Value;
            Status = e.Element("PR").Element("ROW").Element("Status").Value;
            ClientID = e.Element("PR").Element("ROW").Element("ClientID").Value;
            Supervisor = e.Element("PR").Element("ROW").Element("Supervisor").Value;
            Zip = e.Element("PR").Element("ROW").Element("Zip").Value;
            Description = e.Element("PR").Element("ROW").Element("Description").Value;            
            ModDate = e.Element("PR").Element("ROW").Element("ModDate").Value;
            ProjectType = e.Element("PR").Element("ROW").Element("ProjectType").Value;
            CustMetafieldProject = e.Element("PR").Element("ROW").ElementExists("CustMetafieldProject") ? e.Element("PR").Element("ROW").Element("CustMetafieldProject").Value 
                : e.Element("ProjectCustomTabFields").Element("ROW").Element("CustMetafieldProject")?.Value;
            CustMetafieldClientProjectNo = e.Element("PR").Element("ROW").ElementExists("CustMetafieldClientProjectNo") ? e.Element("PR").Element("ROW").Element("CustMetafieldProject").Value
                : e.Element("ProjectCustomTabFields").Element("ROW").Element("CustMetafieldClientProjectNo")?.Value;
            return this;
        }
    }
}