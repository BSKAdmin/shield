﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.DAL.MetaField.Models
{
    public class Collaborator
    {
        public string id { get; set; }
        public string contactID { get; set; }
        public string status { get; set; }
        public string roleName { get; set; }
        public string projectID { get; set; }
        public Collaborator() {}
    }
}