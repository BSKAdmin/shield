﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.DAL.MetaField.Models
{
    public class WBS
    {
        public string id { get; set; }        
        public string parentID { get; set; }        
        public string projectID { get; set; }
        public int hierarchyNumber { get; set; }
        public string parentCode { get; set; }
        public string status { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string comments { get; set; }        
    }
}