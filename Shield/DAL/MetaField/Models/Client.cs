﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.DAL.MetaField.Models
{
    public class Client
    {
        public string id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string status { get; set; }    
    }
}