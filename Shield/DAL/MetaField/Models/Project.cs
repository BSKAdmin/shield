﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.DAL.MetaField.Models
{
    public class Project
    {
        public string id { get; set; }
        public string state { get; set; }
        public string serviceDescription { get; set; }
        public string comments { get; set; }
        public string projectOwnerID { get; set; }
        public string clientProjectNumber { get; set; }
        public string clientID { get; set; }
        public string status { get; set; }
        public string projectManagerID { get; set; }
        public string postalCode { get; set; }
        public string city { get; set; }
        public string address2 { get; set; }
        public string address { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string officeID { get; set; }
        // public Collaborator[] collaborators { get; set; }

        public Project()
        {
            // collaborators = new Collaborator[100];
        }



    }
}