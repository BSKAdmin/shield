﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.DAL.MetaField.Models
{
    public class Contact
    {
        public string id { get; set; }
        public string clientID { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string eMail { get; set; }
        public string workPhone { get; set; }
        public string status { get; set; }
    }
}