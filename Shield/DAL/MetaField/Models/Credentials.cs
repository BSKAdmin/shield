﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.DAL.MetaField.Models
{
    public class Credentials
    {
        public string username { get; set; }
        public string password { get; set; }

        public Credentials() { }
    }
}