﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.DAL.MetaField.DTO
{
    public class GetDTO<T>
    {
        public List<T> records { get; set; }
        public int total { get; set; }

        public GetDTO(){}
        
    }
}