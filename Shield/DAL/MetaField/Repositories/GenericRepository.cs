﻿using Newtonsoft.Json;
using RestSharp;
using Shield.DAL.MetaField.DTO;
using Shield.Services.DataSync;
using Shield.Services.MetaField;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;

namespace Shield.DAL.MetaField.Repositories
{
    public class GenericRepository<T> : IRepository<T>
    {        
        protected RestClient _restClient;
        private readonly string _resource;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resource">This is the route the api will target</param>
        public GenericRepository(string resource = ""){
            
            _restClient = new RestClient();
            _resource = resource;
            //Add Auth Header
            _restClient.AddDefaultHeader("X-MetaField-SessionToken", Authenticator.AuthenticationToken);
            _restClient.AddDefaultHeader("Content-Type", "application/json");
            _restClient.BaseUrl = new Uri(ConfigurationManager.AppSettings["MetaFieldURL"]);
        }

        /// <summary>
        /// 
        /// </summary>        
        /// <returns></returns>
        public virtual async Task<GetDTO<T>> Get(int? level = null, int skip = 0, int limit = 10)
        {
            RestRequest request = new RestRequest(_resource, Method.GET);
            request.AddQueryParameter("skip", skip.ToString());
            request.AddQueryParameter("limit", limit.ToString());
            if (level.HasValue)
            {
                request.AddQueryParameter("level", level.Value.ToString());
            }

            var response = _restClient.Execute<GetDTO<T>>(request);

            if(response.IsSuccessful)
            {
                return response.Data;                
            }            
            return null;
        }
        

        /// <summary>
        /// Creates items if no id or no matching id given
        /// Updates items if item with matching id is found
        /// </summary>
        /// <param name="target"></param>
        /// <param name="Records">List of records to be created or updated</param>
        /// <returns></returns>
        public virtual async Task<bool> Post(List<T> Records)
        {
            try
            {
                //If no records just return true
                if (Records.Any())
                {
                    RestRequest request = new RestRequest(_resource, Method.POST);
                    request.RequestFormat = DataFormat.Json;
                    request.AddBody(Records);

                    var response = _restClient.Execute(request);
                    //var response = await _client.PostAsync(_client.BaseAddress, json );

                    if (!response.IsSuccessful)
                    {
                        //Log response error
                        await LogService.LogError<T>(response.Content, Records);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
            
            
        }
    }
}