﻿using Shield.DAL.MetaField.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Shield.DAL.MetaField.Repositories
{
    public interface IRepository<T>
    {
        Task<GetDTO<T>> Get(int? level = null, int skip = 0, int limit = 10);
        Task<bool> Post(List<T> Records);        
    }
}