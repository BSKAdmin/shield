﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
 using Newtonsoft.Json.Converters;
 using Shield.DAL.ProjectSync.Models;

namespace Shield.DAL.ProjectSync
{
    public class ShieldSyncContext : DbContext
    {
        public ShieldSyncContext()
            : base("ShieldSyncDB")
        {          

        }


        //Using data models from MetaField
        public DbSet<SyncedProject> Project { get; set; }
        public DbSet<SyncedClient> Client { get; set; }
        public DbSet<Run> Run { get; set; }
        public DbSet<SyncLog> Logs { get; set; }
        public DbSet<SyncedEmployee> Employees { get; set; }
        public DbSet<SyncedContact> Contacts { get; set; }
        public DbSet<Branch> Branches { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<SyncedProject>()
                .HasKey(p => new { p.WBS1, p.WBS2, p.WBS3 });

            modelBuilder.Entity<SyncedClient>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Run>()
                .HasKey(r => new { r.Start, r.End });

            modelBuilder.Entity<Branch>()
                .HasKey(e => e.Id);

            modelBuilder.Entity<Branch>()
                .Property(e => e.ProjectIdentifier).HasMaxLength(1);
            modelBuilder.Entity<Branch>()
                .HasIndex(e => e.ProjectIdentifier)
                .IsUnique();
            


        }

    }
}
