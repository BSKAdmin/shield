namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSyncLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SyncLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Occured = c.DateTime(nullable: false),
                        Error = c.String(),
                        ItemsEffected = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SyncLogs");
        }
    }
}
