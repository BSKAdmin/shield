// <auto-generated />
namespace Shield.DAL.ProjectSync.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class UpdatedSyncedProject : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UpdatedSyncedProject));
        
        string IMigrationMetadata.Id
        {
            get { return "201806230008007_UpdatedSyncedProject"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
