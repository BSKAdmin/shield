namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ShieldClients",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Address = c.String(),
                        City = c.String(),
                        State = c.String(),
                        PostalCode = c.String(),
                        NeedsSync = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SyncedProjects",
                c => new
                    {
                        WBS1 = c.String(nullable: false, maxLength: 128),
                        WBS2 = c.String(nullable: false, maxLength: 128),
                        WBS3 = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        ProjMgr = c.String(),
                        ClientID = c.String(),
                        Status = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Zip = c.String(),
                        Supervisor = c.String(),
                        Description = c.String(),
                        NeedsSync = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.WBS1, t.WBS2, t.WBS3 });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SyncedProjects");
            DropTable("dbo.ShieldClients");
        }
    }
}
