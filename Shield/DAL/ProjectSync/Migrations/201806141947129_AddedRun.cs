namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRun : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Runs",
                c => new
                    {
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Error = c.String(),
                    })
                .PrimaryKey(t => new { t.Start, t.End });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Runs");
        }
    }
}
