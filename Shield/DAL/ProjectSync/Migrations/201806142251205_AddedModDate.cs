namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedModDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ShieldClients", "ModDate", c => c.String());
            AddColumn("dbo.SyncedProjects", "ModDate", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SyncedProjects", "ModDate");
            DropColumn("dbo.ShieldClients", "ModDate");
        }
    }
}
