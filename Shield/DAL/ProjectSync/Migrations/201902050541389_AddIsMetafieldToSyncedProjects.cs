namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsMetafieldToSyncedProjects : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SyncedProjects", "IsMetafield", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SyncedProjects", "IsMetafield");
        }
    }
}
