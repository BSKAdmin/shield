namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProjectNumberToSyncedProjects : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SyncedProjects", "ProjectNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SyncedProjects", "ProjectNumber");
        }
    }
}
