namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSyncLog1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SyncLogs", "Type", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SyncLogs", "Type");
        }
    }
}
