﻿namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBranchesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProjectIdentifier = c.String(maxLength: 1),
                        FolderStrategy = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ProjectIdentifier, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Branches", new[] { "ProjectIdentifier" });
            DropTable("dbo.Branches");
        }
    }
}
