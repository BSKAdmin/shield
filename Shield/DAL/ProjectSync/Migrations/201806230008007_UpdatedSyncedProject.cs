namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedSyncedProject : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SyncedProjects", "ProjectType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SyncedProjects", "ProjectType");
        }
    }
}
