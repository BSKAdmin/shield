namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedShieldClientToSyncedClient : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ShieldClients", newName: "SyncedClients");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.SyncedClients", newName: "ShieldClients");
        }
    }
}
