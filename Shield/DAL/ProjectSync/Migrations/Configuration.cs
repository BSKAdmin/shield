using System.Collections.Generic;
using Shield.DAL.ProjectSync.Models;

namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Shield.DAL.ProjectSync.ShieldSyncContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"DAL\ProjectSync\Migrations";
        }

        protected override void Seed(ShieldSyncContext context)
        {
            context.Branches.AddOrUpdate(new Branch()
                    { Name = "Bakersfield", ProjectIdentifier = "B", FolderStrategy = FolderStrategy.FileSystem });
            context.Branches.AddOrUpdate(new Branch() { Name = "Fresno", ProjectIdentifier = "F", FolderStrategy = FolderStrategy.FileSystem });
            context.Branches.AddOrUpdate(new Branch()
                { Name = "Livermore", ProjectIdentifier = "L", FolderStrategy = FolderStrategy.FileSystem });
            context.Branches.AddOrUpdate(new Branch()
                { Name = "Sacramento", ProjectIdentifier = "S", FolderStrategy = FolderStrategy.FileSystem });
            context.SaveChanges();
        }
    }
}
