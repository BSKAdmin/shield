namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedContact : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SyncedContacts",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ClientID = c.String(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        EMail = c.String(nullable: false),
                        WorkPhone = c.String(),
                        Status = c.String(nullable: false),
                        ModDate = c.String(nullable: false),
                        NeedsSync = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SyncedContacts");
        }
    }
}
