namespace Shield.DAL.ProjectSync.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSyncedEmployee : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SyncedEmployees",
                c => new
                    {
                        EmployeeID = c.String(nullable: false, maxLength: 35),
                        FirstName = c.String(maxLength: 25),
                        LastName = c.String(maxLength: 25),
                        EMail = c.String(maxLength: 35),
                        Status = c.String(maxLength: 1),
                        ModDate = c.String(),
                        NeedsSync = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeeID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SyncedEmployees");
        }
    }
}
