﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.DAL.ProjectSync.Models
{
    public class SyncedProject
    {        
        public string WBS1 { get; set; }
        public string WBS2 { get; set; }
        public string WBS3 { get; set; }
        public string Name { get; set; }
        public string ProjMgr { get; set; }
        public string ClientID { get; set; }
        public string Status { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Supervisor { get; set; }
        public string Description { get; set; }
        public string ModDate { get; set; }
        public string ProjectType { get; set; }
        public string ProjectNumber { get; set; }
        public bool NeedsSync { get; set; }
        public bool IsMetafield { get; set; }
    }
}