﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.DAL.ProjectSync.Models
{
    public class Run
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string Error { get; set; }
    }
}