﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shield.DAL.ProjectSync.Models
{
    public class SyncedEmployee
    {
        [Key]
        [MaxLength(35)]
        public string EmployeeID { get; set; }
        [MaxLength(25)]
        public string FirstName { get; set; }
        [MaxLength(25)]
        public string LastName { get; set; }
        [MaxLength(35)]
        public string EMail { get; set; }
        [MaxLength(1)]
        public string Status { get; set; }
        public string ModDate { get; set; }
        public bool NeedsSync { get; set; }
    }
}