﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shield.DAL.ProjectSync.Models
{
    public class SyncedContact
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string ClientID { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string EMail { get; set; }
        public string WorkPhone { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        public string ModDate { get; set; }
        [Required]
        public bool NeedsSync { get; set; }
    }
}