﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shield.DAL.ProjectSync.Models
{
    public class SyncLog
    {
        [Key]
        public int Id { get; set; }
        public DateTime Occured { get; set; }
        public string Error { get; set; }
        public string ItemsEffected { get; set; }
        [MaxLength(50)]
        public string Type { get; set; }
    }
}