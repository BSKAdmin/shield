﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shield.DAL.ProjectSync.Models
{
    public class Branch
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProjectIdentifier { get; set; }
        public FolderStrategy FolderStrategy { get; set; }
    }

    public enum FolderStrategy
    {
        FileSystem,
        SharePoint
    }
}