﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.Models.DTO
{
    [Serializable]
    public class PwxmlDTO
    {
        public string QueryName { get; set; }
        public List<StoredProcedureParameter> Parameters { get; set; }

    }

    public class StoredProcedureParameter
    {
        public string ParamName { get; set; }
        public object ParamValue { get; set; }
    }
}