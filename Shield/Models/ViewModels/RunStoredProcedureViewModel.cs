﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Shield.Models.ViewModels
{
    public class RunStoredProcedureViewModel
    {
        public List<string> StoredProcedures { get; set; }
        public XDocument StoredProcedureList { get; set; }
        public List<StoredProcedureParameter> Parameters { get; set; }
        public XDocument Result { get; set; }
        public string SelectedStoredProcedure { get; set; }
    }

    public class StoredProcedureParameter
    {
        public int ParameterId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterDataType { get; set; }
        public string ParameterValue { get; set; }
    }
}