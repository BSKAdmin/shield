﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shield.Models.ViewModels
{
    public class PostRunStoredProcedureViewModel
    {               
        public List<StoredProcedureParameter> Parameters { get; set; }       
        public string SelectedStoredProcedure { get; set; }
    }
}