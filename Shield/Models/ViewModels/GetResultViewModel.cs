﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Shield.Models.ViewModels
{
    public class GetResultViewModel
    {
        public XDocument Result { get; set; }
        public List<string> Headers { get; set; }
        public List<List<string>> Data { get; set; }

        public GetResultViewModel() { }
        public GetResultViewModel(XDocument document)
        {
            Result = document;
            Headers = GetHeadersFromDocument(document);
            Data = GetDataFromDocument(document, GetHeadersFromDocument(document));
        }

        /// <summary>
        /// Get headers from XDocument returned from Deltek
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        private List<string> GetHeadersFromDocument(XDocument document) {
            List<string> headers = new List<string>();
            var root = document.Nodes();
            XNamespace xs = "http://www.w3.org/2001/XMLSchema";
            var sequence = document.Descendants(xs + "element");            
            
            foreach (var s in sequence)
            {
                if (s.FirstAttribute.Value != "Table" && s.FirstAttribute.Value != "NewDataSet")
                {
                    headers.Add(s.FirstAttribute.Value);
                }
            }
            return headers;
        }

        private List<List<string>> GetDataFromDocument(XDocument document, List<string> headers)
        {
            List<List<string>> rows = new List<List<string>>();
            var sequence = document.Descendants("Table");
            
            foreach(var s in sequence)
            {
                List<string> row = new List<string>();
                foreach(var h in headers)
                {
                    if (!s.ElementExists(h))
                    {
                        row.Add("");
                    }
                    else
                    {
                        row.Add(s.Element(h).Value);
                    }
                }
                //foreach(var e in s.Elements())
                //{
                //    row.Add(e.Value);
                //}
                rows.Add(row);
            }

            return rows;
        }
    }
}