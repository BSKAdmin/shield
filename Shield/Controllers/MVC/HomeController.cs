﻿using Shield.Models.ViewModels;
using Shield.Services.Deltek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Shield.Controllers.MVC
{
    public class HomeController : Controller
    {
        private DeltekService _deltek = new DeltekService();

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            var document = _deltek.GetResourceFromStoredProcedure("sp_GetStoredProcs", null);
            return View(document);
        }

        [HttpPost]
        public ActionResult GetResult (PostRunStoredProcedureViewModel postedObject)
        {
            var parameters = new Dictionary<string, object>();
            foreach(var param in postedObject.Parameters){
                parameters.Add(param.ParameterName, param.ParameterValue);
            }
            var result = _deltek.GetResourceFromStoredProcedure(postedObject.SelectedStoredProcedure, parameters);
            return View(new GetResultViewModel(result));
        }
        
        
        /// <summary>
        /// Load view with selected stored procedure
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <returns></returns>        
        public ActionResult StoredProcedures(string storedProcedure = null)
        {
            RunStoredProcedureViewModel viewModel = new RunStoredProcedureViewModel();
            ViewBag.Title = "Run Stored Procedure";
            ViewBag.storedProcedure = storedProcedure;
            viewModel.SelectedStoredProcedure = storedProcedure;
            viewModel.StoredProcedures = GetStoredProcedures();

            if (storedProcedure != null)
            {
                viewModel.Parameters = GetStoredProcedureParameters(storedProcedure);
            }

            viewModel.Result = null;
            return View(viewModel);
        }

        /// <summary>
        /// Returns a list of available stored procedures
        /// </summary>
        /// <returns></returns>
        private List<string> GetStoredProcedures()
        {
            var storedProcedureList = _deltek.GetResourceFromStoredProcedure("sp_GetStoredProcs", null);
            return storedProcedureList.Element("NewDataSet")
                                      .Elements("Table")
                                      .GroupBy(g => g.Element("ObjectName"))
                                      .Select(g => g.Key.Value.Replace("DeltekStoredProc_", ""))
                                      .Distinct()
                                      .ToList();
        }

        /// <summary>
        /// Returns a list of parameters needed to run a particular storedProcedure
        /// </summary>
        /// <param name="storedProcedure">Name of the stored procedure</param>
        /// <returns></returns>
        private List<StoredProcedureParameter> GetStoredProcedureParameters(string storedProcedure)
        {
            var storedProcedureList = _deltek.GetResourceFromStoredProcedure("sp_GetStoredProcs", null);
            var parameters = new List<StoredProcedureParameter>();
            storedProcedureList.Element("NewDataSet")
                               .Elements("Table")                               
                               .Where(x => x.Element("ObjectName").Value.EndsWith(storedProcedure))
                               .ToList()
                               .ForEach(x => {
                                   parameters.Add(new StoredProcedureParameter()
                                   {
                                       ParameterId = int.Parse(x.Element("ParameterID").Value),
                                       ParameterName = x.Element("ParameterName").Value,
                                       ParameterDataType = x.Element("ParameterDataType").Value
                                   });
                               });
            return parameters;
        }
        
    }
}
