﻿using Shield.Services.Deltek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Shield.Controllers.API
{    
    public class VendorsController : ApiController
    {
        private VendorService _vendors;

        public VendorsController()
        {
            _vendors = new VendorService();
        }

        /// <summary>
        /// Search Vendors by specifying the search term
        /// </summary>
        /// <param name="searchTerm">matches this string to the name of vendors</param>
        /// <returns>List of vendors</returns>
        [Route("api/Vendors")]
        [HttpGet]        
        public IHttpActionResult VendorSearch([FromUri]string searchTerm)
        {
            try
            {
                var result = _vendors.GetVendorAddress(searchTerm);
                if (result.Any()) return Ok(result);
                return NotFound();
            }
            catch(Exception e)
            {
                ModelState.AddModelError("Error", "Could not get vendors");
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Retrieves multiple vendors by their keys
        /// </summary>
        /// <param name="keys">List of vendor keys(VendorAddress.Vendor)</param>
        /// <returns></returns>
        [Route("api/Vendors")]
        [HttpGet]
        public IHttpActionResult Vendors([FromUri]List<string> keys)
        {
            try
            {
                var key = keys.FirstOrDefault();
                var split = key != null ? key.Split(',') : null;
                var result = _vendors.GetVendors(split);
                if (result.Any()) return Ok(result);
                return NotFound();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", "Could not get vendors");
                return BadRequest(ModelState);
            }
        }

        [Route("api/Vendors")]
        [HttpGet]
        public IHttpActionResult Vendor([FromUri]string key)
        {
            try
            {
                var result = _vendors.GetVendor(key);
                if (result != null) return Ok(result);
                return NotFound();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Error", "Could not get vendors");
                return BadRequest(ModelState);
            }
        }
    }
}