﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using Shield.Models.DTO;
using System.Web.Http.Description;
using System.IO;
using System.Text;
using System.Web;

namespace Shield.Controllers.API
{
    public class PWXMLController : ApiController
    {
        private VisionAPIRepository _repository;

        // GET: api/PWXML
        //public VisionMessage Get()
        //{
        //    repository = new VisionAPIRepository(
        //        ConfigurationManager.AppSettings["VisionWSUrl"],
        //         ConfigurationManager.AppSettings["VisionDatabase"],
        //         ConfigurationManager.AppSettings["VisionUser"],
        //         ConfigurationManager.AppSettings["VisionPassword"]);

        //    VisionMessage l_VisionMessage = repository.GetUDICRecordsByQuery("UDIC_PrevailingWage", "SELECT upwrg.*  FROM dbo.UDIC_PrevailingWage, UDIC_PrevailingWage_RateGrid upwrg", RecordDetail.AllPrimary);
        //   // VisionMessage l_VisionMessage = repository.GetRecordsByQuery(VisionInfoCenters.Employees, "Select * from EM where employee = '1901'", RecordDetail.Primary);


        //    string l_Return = l_VisionMessage.ReturnedData.ToString();


        //    return l_VisionMessage;

        //    }

        //public VisionMessage Get()
        //{
        //    repository = new VisionAPIRepository(
        //        ConfigurationManager.AppSettings["VisionWSUrl"],
        //         ConfigurationManager.AppSettings["VisionDatabase"],
        //         ConfigurationManager.AppSettings["VisionUser"],
        //         ConfigurationManager.AppSettings["VisionPassword"]);

        //    Dictionary<string, object> l_Parameters = new Dictionary<string, object>();
        //    l_Parameters.Add("pAsOfDate", "05/17/2018");


        //    VisionMessage l_VisionMessage = repository.ExecuteStoredProcedure("sp_PWGetProjectToProcess", l_Parameters);
        //    // VisionMessage l_VisionMessage = repository.GetRecordsByQuery(VisionInfoCenters.Employees, "Select * from EM where employee = '1901'", RecordDetail.Primary);


        //    string l_Return = l_VisionMessage.ReturnedData.ToString();


        //    return l_VisionMessage;

        //}

        //// GET: api/PWXML/5
        //public VisionMessage GetEEData(int id)
        //{
        //    repository = new VisionAPIRepository(
        //       ConfigurationManager.AppSettings["VisionWSUrl"],
        //        ConfigurationManager.AppSettings["VisionDatabase"],
        //        ConfigurationManager.AppSettings["VisionUser"],
        //        ConfigurationManager.AppSettings["VisionPassword"]);

        //    VisionMessage l_VisionMessage = repository.GetRecordsByKey(VisionInfoCenters.Employees.ToString(), VisionKey.CreateGenericKey(id.ToString()));

        //    string l_Return = l_VisionMessage.ReturnedData.ToString();


        //    return l_VisionMessage;
        //}



        // POST: api/PWXML
        [ResponseType(typeof(VisionMessage))]
        public  VisionMessage Post([FromBody]PwxmlDTO p_PwxmlDTO)
        {
            VisionMessage l_VisionMessage = new VisionMessage();

            string body = Request.Content.ReadAsStringAsync().Result;


            //if (!ModelState.IsValid)
            //{
            //    l_VisionMessage.Errors.Add("Bad Model", "Bad Model");
            //    return l_VisionMessage;
            //}


            _repository = new VisionAPIRepository(
                     ConfigurationManager.AppSettings["VisionWSUrl"],
                     ConfigurationManager.AppSettings["VisionDatabase"],
                     ConfigurationManager.AppSettings["VisionUser"],
                     ConfigurationManager.AppSettings["VisionPassword"]);


            if (p_PwxmlDTO.Parameters != null)
            {
                Dictionary<string, object> l_ParamDictionary = new Dictionary<string, object>();
                foreach (var param in p_PwxmlDTO.Parameters)
                {
                    l_ParamDictionary.Add(param.ParamName, param.ParamValue);
                }
                l_VisionMessage = _repository.ExecuteStoredProcedure(p_PwxmlDTO.QueryName, l_ParamDictionary);
            }
            else
            {
                l_VisionMessage = _repository.ExecuteStoredProcedure(p_PwxmlDTO.QueryName);
            }

            

            return l_VisionMessage;
        }

        private string GetDocumentContents(System.Web.HttpRequestBase Request)
        {
            string documentContents;
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }
            return documentContents;
        }
    }
}
