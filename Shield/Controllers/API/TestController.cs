﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Shield.DeltekCloudService;
using System.Configuration;
namespace Shield.Controllers.API
{
    public class TestController : ApiController
    {
        private VisionAPIRepository repository;

        // GET: api/Test
        public string Get()
        {
            repository = new VisionAPIRepository(
              ConfigurationManager.AppSettings["VisionWSUrl"],
               ConfigurationManager.AppSettings["VisionDatabase"],
               ConfigurationManager.AppSettings["VisionUser"],
               ConfigurationManager.AppSettings["VisionPassword"]);

            string l_test = repository.GetCurrentUserInfo();

            return l_test;
        }

        // GET: api/Test/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Test
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Test/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Test/5
        public void Delete(int id)
        {
        }
    }
}
