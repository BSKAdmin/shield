﻿using Shield.Services.Deltek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Shield.Controllers.API
{
    public class StoredProcedureController : ApiController
    {
        private DeltekService _deltek;        
        public StoredProcedureController()
        {
            _deltek = new DeltekService();
        }
        /// <summary>
        /// Runs a stored procedure that is in Deltek given the required parameter.
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="Parameters"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/StoredProcedure/{storedProcedure}")]
        public IHttpActionResult RunStoredProcedure([FromUri] string storedProcedure, [FromBody]Dictionary<string, object> parameters)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                return Ok(_deltek.GetResourceFromStoredProcedure(storedProcedure, parameters));
            }
            catch(Exception e)
            {
                ModelState.AddModelError("Error", $"Could not run {storedProcedure}");
                return BadRequest(ModelState);
            }

        }

    }
}