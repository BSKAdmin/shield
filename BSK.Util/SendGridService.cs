﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BSK.Util
{
    public class SendGridService : ISendGridService
    {
        private string _apiKey;

        public SendGridService(string key)
        {
            _apiKey = key;

        }
        public async Task<bool> SendMessage(EmailAddress from,
                                            List<EmailAddress> toList,
                                            List<EmailAddress> ccList,
                                            string textMessage,
                                            string htmlMessage,
                                            string subject)
        {
            var client = new SendGridClient(_apiKey);
            var msg = new SendGridMessage()
            {
                From = from,
                Subject = subject,
                PlainTextContent = textMessage,
                HtmlContent = htmlMessage,                
            };
            msg.AddTos(toList);
            if (ccList != null) msg.AddCcs(ccList);
            var response = await client.SendEmailAsync(msg);            
            return response.StatusCode == System.Net.HttpStatusCode.Accepted;
        }
    }

    public interface ISendGridService
    {
        Task<bool> SendMessage(EmailAddress from, List<EmailAddress> toList, List<EmailAddress> ccList, string textMessage, string htmlMessage, string subject);
    }
}
