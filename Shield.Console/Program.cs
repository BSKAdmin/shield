﻿using BSK.Util;
using SendGrid.Helpers.Mail;
using Shield.DAL.ProjectSync;
using Shield.Services.DataSync;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shield.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            SyncService _syncService = new SyncService();
            ShieldSyncContext context = new ShieldSyncContext();
            var run = _syncService.RunSync().GetAwaiter().GetResult();
            var errors = context.Logs.Where(l => l.Occured >= run.Start && l.Occured <= run.End).ToList();
            
            SendGridService sg = new SendGridService(ConfigurationManager.AppSettings["SendGridKey"]);


            List<EmailAddress> toList = new List<EmailAddress>();

            if(args[0] != null && args[1] != null)
            {
                toList.Add( 
                    new EmailAddress()
                    {
                        Name = args[0],
                        Email = args[1]
                    }
                );
            }
            else
            {
                toList.Add(
                    new EmailAddress()
                    {
                        Name = "IT",
                        Email = "it@bskassociates.com"
                    });
            }
            EmailAddress from = new EmailAddress()
            {
                Name = "Metafield Sync Errors",
                Email = "MetaFieldSyncErrors@bskassociates.com"
            };

            var htmlMessage = "If you wish to stop receiving these emails resolve the following errors:";

            foreach (var error in errors)
            {
                htmlMessage += $@"
                    <h2>{error.Type}</h3>
                    <h4>{error.Occured.ToLongTimeString()}</h4>
                    <h3>Error</h3>
                    <pre>{error.Error}</pre>
                    <h3>Items Effected:<h3>
                    <pre>{error.ItemsEffected}</pre>
                ";
            }
            var textMessage = htmlMessage;
            if (errors.Any())
            {
                var sentEmail = sg.SendMessage(from, toList, null, textMessage, htmlMessage, "Metafield Sync Errors").GetAwaiter().GetResult();
            }
            else
            {
                var sentEmail = sg.SendMessage(from, toList, null, "Sync Completed", "Sync Completed", "Metafield Synced with no Errors").GetAwaiter().GetResult();
            }
        }
    }
}
