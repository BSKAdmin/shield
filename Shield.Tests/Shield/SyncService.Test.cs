﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shield.DAL.Deltek.Models;
using Shield.DAL.ProjectSync.Models;
using Shield.Services.DataSync;
using Shield.Services.Deltek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shield.Tests.Shield
{
    [TestClass]
    public class SyncServiceTest
    {
        private ProjectService _projects = new ProjectService();
        private ClientService _clients = new ClientService();
        private EmployeeService _employees = new EmployeeService();
        private SyncService syncService = new SyncService();

        [TestMethod]
        public void GetNewProjects()
        {
            //arrange            
            var deltekProjects = _projects.GetProjects();
            //act
            var results = syncService.DiscoverNewProjects(deltekProjects);
            //assert
            Assert.IsInstanceOfType(results, typeof(List<Project>));
            Assert.IsNotNull(results);
            Assert.IsTrue(results.Count() >= 0);
        }

        [TestMethod]
        
        public void SyncASingleProjectAndSubProjects()
        {
            var deltekProjects = _projects.GetProject("G1830411F");

            //Save projects to database
            syncService.ForceUpdateProjects(deltekProjects);

            Assert.IsTrue(deltekProjects.Any());
        }

        [TestMethod]
        public void ShouldCreateNewFolders()
        {
            Project l_Project = new Project()
            {
                Name = "TestProjectF",
                WBS1 = "G1900211F"
            };
            syncService.CreateFolder(l_Project);
            l_Project = new Project()
            {
                Name = "TestProjectB",
                WBS1 = "G1823211B"
            };
            syncService.CreateFolder(l_Project);
            l_Project = new Project()
            {
                Name = "TestProjectS",
                WBS1 = "00000000"
            };
            syncService.CreateFolder(l_Project);
            l_Project = new Project()
            {
                Name = "TestProjectP",
                WBS1 = "00000000"
            };
            syncService.CreateFolder(l_Project);
            l_Project = new Project()
            {
                Name = "TestProjectL",
                WBS1 = "G1824011L"
            };
            syncService.CreateFolder(l_Project);
            
        }

        [TestMethod]
        public void TestSyncNewDataWithRunDate()
        {
            //arrange
            var lastRun = new Run() { Start = new DateTime(2020, 7, 20)};
            var deltekProjects = _projects.GetProjects(lastRun.Start);
            //act
            syncService.SyncNewData(deltekProjects, lastRun);
            //assert
            //Run without failing.
        }

        [TestMethod]
        public void TestSyncNewEmployee()
        {
            //arrange        
            var deltekEmployees = _employees.GetEmployees();
            //act
            syncService.UpdateEmployees(deltekEmployees);
            //assert
            //Run without failing.
        }

        [TestMethod]
        public void TestSyncNewData()
        {
            //arrange        
            var deltekProjects = _projects.GetProjects();
            //act
            syncService.SyncNewData(deltekProjects, null);
            //assert
            //Run without failing.
        }

        [TestMethod]
        public void TestUpdateData()
        {
            var lastRun = new Run() { Start = DateTime.Now };
            var deltekProjects = _projects.GetProjects(lastRun.Start);
            var deltekClients = _clients.GetClients(lastRun.Start);

            syncService.UpdateData(deltekProjects, deltekClients);
        }

        [TestMethod]
        public async Task TestSyncClientToMetafield()
        {
            await syncService.SyncClientsToMetafield();
        }

        [TestMethod]
        public async Task TestSyncProjectsToMetafield()
        {
            await syncService.SyncProjectsToMetafield();
        }

        [TestMethod]
        public async Task TestSyncWBSToMetafield()
        {
            await syncService.SyncWBSToMetafield();
        }

        [TestMethod]
        public async Task TestSyncContactsToMetafield()
        {
            await syncService.SyncContactsToMetafield();
        }

        [TestMethod]
        public async Task TestSyncEmployeesToMetafield()
        {
            await syncService.SyncEmployeesToMetafield();
        }

        [TestMethod]
        public async Task TestRunAsync()
        {
            await syncService.RunSync();
        }

        [TestMethod]
        public void TestInitialize()
        {
            syncService.Initialize();
        }

    }
}
