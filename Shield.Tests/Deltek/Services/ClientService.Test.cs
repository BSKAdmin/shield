﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shield.DAL.Deltek.Models;
using Shield.Services.Deltek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;

namespace Shield.Tests.Deltek.Services
{
	[TestClass]
    public class ClientServiceTest
    {
        ClientService _clients;
		public ClientServiceTest()
        {
            _clients = new ClientService();
        }

		[TestMethod]
		public void TestGetClients()
        {
            var result = _clients.GetClients();
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<Client>));
        }
    }
}
