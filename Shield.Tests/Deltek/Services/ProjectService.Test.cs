﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shield.DAL.Deltek.Models;
using Shield.Services.Deltek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shield.Tests.Deltek.Services
{
    [TestClass]
    public class ProjectServiceTest
    {
        private ProjectService _projects;
        public ProjectServiceTest()
        {
            _projects = new ProjectService();
        }

        [TestMethod]
        public void TestGetProjects()
        {
            var result = _projects.GetProjects();

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<Project>));
        }
    }
}
