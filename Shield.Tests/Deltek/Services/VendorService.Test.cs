﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shield.DAL.Deltek.Models;
using Shield.Services.Deltek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shield.Tests.Deltek.Services
{
    [TestClass]
    public class VendorServiceTest
    {
        private VendorService _vendors;
        public VendorServiceTest()
        {
            _vendors = new VendorService();
        }

        [TestMethod]
        public void GetVendorAddressEmpty()
        {
            var result = _vendors.GetVendorAddress("");
            Assert.IsFalse(result.Any());
        }


        [TestMethod]
        public void GetVendorAddress()
        {
            var result = _vendors.GetVendorAddress("Ide");
            //Assert.IsTrue(result.Any()); //Can be empty
            Assert.IsInstanceOfType(result, typeof(List<VendorAddress>));
        }

        [TestMethod]
        public void GetVendorAddressByID()
        {
            var result = _vendors.GetVendor("VWR");
            //Assert.IsTrue(result.Any()); //Can be empty
            Assert.IsInstanceOfType(result, typeof(List<VendorAddress>));
        }
    }
}
