﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shield.DAL.Deltek.Models;
using Shield.Services.Deltek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shield.Tests.Deltek.Services
{
    [TestClass]
    public class EmployeeServiceTest
    {
        private EmployeeService _employees = new EmployeeService();

        [TestMethod]
        public void GetEmployeeTest()
        {
            var result = _employees.GetEmployees();
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<Employee>));            
            
        }
    }
}
