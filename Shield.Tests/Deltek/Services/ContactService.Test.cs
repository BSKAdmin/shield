﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shield.DAL.Deltek.Models;
using Shield.Services.Deltek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shield.Tests.Deltek.Services
{
    [TestClass]
    public class ContactServiceTest
    {
        ContactService _contacts;
        public ContactServiceTest()
        {
            _contacts = new ContactService();
        }

        [TestMethod]
        public void TestGetContactsByModDate()
        {
            var result = _contacts.GetContact(new DateTime(2018,05,01));
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<Contact>));
        }

        [TestMethod]
        public void TestGetContactsByClientIDs()
        {
            var result = _contacts.GetContact(new List<string>() {
                "Blueline",
                "SCHILDERS1220649716818",
                "SCHILDERS1201731915320"
            });
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<Contact>));
        }
    }
}
