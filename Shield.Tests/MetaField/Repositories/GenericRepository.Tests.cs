﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Helpers;
using Shield.DAL.MetaField.Models;
using Shield.DAL.MetaField.Repositories;

namespace Shield.Tests.MetaField.Repositories
{
    [TestClass]
    public class GenericRepositoryTests
    {
        private GenericRepository<Client> _clientRepo = new GenericRepository<Client>("clients");
        private GenericRepository<Contact> _contactRepo = new GenericRepository<Contact>("contacts");
        private GenericRepository<Employee> _employeeRepo = new GenericRepository<Employee>("employees");
        private GenericRepository<Project> _projectRepo = new GenericRepository<Project>("projects");
        private GenericRepository<WBS> _wbsRepo = new GenericRepository<WBS>("wbs");
        [TestMethod]
        public async Task TestGet()
        {
            var response = await _clientRepo.Get();
            var response1 = await _contactRepo.Get();
            var response2 = await _employeeRepo.Get();
            var response3 = await _projectRepo.Get();
            var response4 = await _wbsRepo.Get(1); //Level required or it will fail.
            Assert.IsNotNull(response);
            Assert.IsNotNull(response1);
            Assert.IsNotNull(response2);
            Assert.IsNotNull(response3);
            Assert.IsNotNull(response4);
        }

        [TestMethod]
        public async Task TestGetClients()
        {
            List<Client> badClients = new List<Client>();
            string badClientsJson = "[";
            for (int i = 0; i <= 1000; i += 100)
            {
                var response = await _clientRepo.Get(skip: i, limit: 100);
                foreach (var record in response.records)
                {
                    if (record.name.StartsWith("Roeb", StringComparison.OrdinalIgnoreCase) || record.id.StartsWith("Roeb", StringComparison.OrdinalIgnoreCase)|| record.address.Contains("Default"))
                    {
                        badClients.Add(record);
                        badClientsJson += $"{Json.Encode(record)}, ";
                    }
                }
                Assert.IsNotNull(response.records);
                
            }

            badClientsJson += "]";
            Assert.IsTrue(badClients.Any());

        }

        [TestMethod]
        public async Task GetProjectsTest()
        {
            List<Project> projects = new List<Project>();
            
            for (int i = 0; i <= 8258; i += 100)
            {
                var response = await _projectRepo.Get(level: 1, skip: i, limit: 100);
                foreach (var record in response.records)
                {
                    projects.Add(record);
                }
                
                Assert.IsNotNull(response.records);
                
            }
            Assert.IsTrue(projects.Any(p => p.code == "C2035760F"));
            
            Assert.IsTrue(projects.Any());
        }

        [TestMethod]
        public async Task TestPost()
        {
            var Client = new Client()
            {            
                id = "Test Client",
                address = "Test Address",
                city = "Test City",
                name = "Test Client",
                status = "Inactive", //the possible values are Active, Inactive, Deleted.
                state = "CA",
                postalCode = "93650"                
            };
            var response = await _clientRepo.Post(new List<Client>() { Client } );
            Assert.IsTrue(response);
        }

        [TestMethod]
        public async Task TestPostTooMany()
        {
            var client = new Client()
            {
                id = "Test Client",
                address = "Test Address",
                city = "Test City",
                name = "Test Client",
                status = "Inactive", //the possible values are Active, Inactive, Deleted.
                state = "CA",
                postalCode = "93650"
            };
            var list = new List<Client>();
            for (var i = 0; i < 101; i++) list.Add(client);
            var response = await _clientRepo.Post(list);
            Assert.IsFalse(response);
        }
    }
}
