﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shield.Services.MetaField;

namespace Shield.Tests.MetaField.Services
{	
	[TestClass]
    public class AuthenticatorTests
    {
		[TestMethod]
		public void GetAuthenticationToken()
        {
            var token = Authenticator.AuthenticationToken;		
            Assert.IsNotNull(token);
        }
    }
}
