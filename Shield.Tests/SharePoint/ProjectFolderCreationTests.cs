﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.SharePoint.Client;
using System.Security;
using System.Linq;
using Shield.DAL.Deltek.Models;
using Shield.Services;
using Shield.Services.SharePoint;
using Folder = Microsoft.SharePoint.Client.Folder;

namespace Shield.Tests.SharePoint
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class ProjectFolderCreationTests
    {
        private List<Project> projects;

        [TestInitialize]
        public void TestSetup()
        {
            projects = new List<Project>
            {
                new Project()
                {
                    WBS1 = "E123455663F",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "G123455663F",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "C123455663F",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "E123455663L",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "G123455663L",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "C123455663L",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "E123455663S",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "C123455663S",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "G123455663S",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "E123455663B",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "C123455663B",
                    Name = "This is a test project"
                },
                new Project()
                {
                    WBS1 = "G123455663B",
                    Name = "This is a test project"
                }

            };
        }
       

        [TestMethod]
        public void SharePointProjectFolderCreation()
        {
            SharePointProjectFolderStrategy projectFolderService = new SharePointProjectFolderStrategy();
            SharePointFolderService folderService = new SharePointFolderService();
            foreach (var project in projects)
            {

                var projectFolder = projectFolderService.CreateProjectFolders(project);
                var folders = folderService.CreateProjectFolder(projectFolder);

                Assert.AreEqual(folders.Count(), 9);
                foreach (var folder in folders)
                {
                    Assert.IsNotNull(folder);
                }
            }


        }

        [TestMethod]
        public void FileSystemProjectFolderCreation()
        {
            FileSystemProjectFolderStrategy projectFolderService = new FileSystemProjectFolderStrategy();
            foreach (var project in projects)
            {
                projectFolderService.CreateProjectFolders(project);
            }
        }

       
    }
}
